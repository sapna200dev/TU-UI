import React, { Component, Fragment } from "react";
import { ViewEmployeeModal } from "../../components/employee";
import { getEmployeeDetails } from "../../services/common";
import { EMPLOYEE_STATE } from "../../constant";
import { getLoggedInUser } from "../../../Comman/functions";
import {  getBranches, getRoles, addEmployee } from "../../services/common";

export class ViewEmployee extends Component {
  state = EMPLOYEE_STATE;

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({
      id
    })
    this.getBankDetails(id);

    const userObj = getLoggedInUser('loggedInUser')
    let user='';

    if (userObj) {
        user = JSON.parse(userObj);
      }

    this.getUserRoles();
    this.getUserBranches(user.id);
    console.log("getEmployeeDetails detailsdddddd");
  }

  getBankDetails = (id) => {
    getEmployeeDetails(id)
      .then((res) => {

        console.log("getEmployeeDetails response ", res.data);
        if (res.data && res.data.status == 200) {
       
          this.setState({
            first_name: res.data.data.first_name,
            last_name: res.data.data.last_name,
            branch_name:res.data.data.branch_name,
            status:res.data.data.status,
            role:res.data.data.role,
            mobile_number:res.data.data.mobile_number,
            email:res.data.data.email,
            username:res.data.data.username,
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
          });
        }
        console.log("bankDetail => ", res);
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
      });
  };



    
  getUserRoles = () => {
    getRoles().then(res => {
      console.log('res roles => ', res.data.data)
      this.setState({
        all_roles : res.data.data
      })
    }).catch(err => {
      console.log('errror roles => ', err)
    })
  }

  getUserBranches = (bank_id) => {
    getBranches(bank_id).then(res => {
      this.setState({
        all_branches : res.data.data
      })
    }).catch(err => {

    })
  }



  render() {
    const { 
        first_name,
        last_name,
        branch_name,
        status,
        role,
        mobile_number,
        email,
        access_level,
        team_lead,
        username,
        password,
        confirm_password,
        handleErrors,
        all_roles,
        all_branches,
        errorMsg,
        successMesg
    } = this.state;

    return (
      <Fragment>
        <div>
          <ViewEmployeeModal
              first_name={first_name}
              last_name={last_name}
              branch_name={branch_name}
              status={status}
              role={role}
              mobile_number={mobile_number}
              email={email}
              access_level={access_level}
              team_lead={team_lead}
              username={username}
              password={password}
              confirm_password={confirm_password}
              handleInputValues={this.handleInputValues}
              handleErrors={handleErrors}
              handleAddEmployee={this.handleAddEmployee}
              all_roles={all_roles}
              all_branches={all_branches}
              successMesg={successMesg}
              errorMsg={errorMsg}
          />
        </div>
      </Fragment>
    );
  }
}
