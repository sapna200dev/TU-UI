import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { EmployeeUpdate } from "../../components/employee";
import { EMPLOYEE_STATE, EMPLOYEE_FORM_VALIDATIONS } from "../../constant";
import { getLoggedInUser } from "../../../Comman/functions";
import {  getEmployeeDetails, updateEmployee } from "../../services/common";
import { getStatues, getBranches, getRoles } from "../../../Comman/actions";

class EmployeeDetails extends Component {
  state = EMPLOYEE_STATE;

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({ id });

    this.getEmployeeById(id);


    this.props.getRoles();
    this.props.getBranches();
    this.props.getStatues();
  }


  componentDidUpdate(prevProps) {
    const { branches, roles, statuses } = this.props;

    if (prevProps.roles != roles) {
      this.setState({ all_roles: roles });
    }
    if (prevProps.branches != branches) {
      this.setState({ all_branches: branches });
    }

    if (prevProps.statuses != statuses) {
      this.setState({ statuses });
    }
  }



    /**
   * Get User branches
   */
  getEmployeeById = (bank_id) => {
    getEmployeeDetails(bank_id)
      .then((res) => {
        console.log('details = ', res.data)
        this.setState({
          first_name:res.data.data.first_name,
          last_name:res.data.data.last_name,
          branch_id:res.data.data.branch_id,
          status:res.data.data.status,
          role_id:res.data.data.role_id,
          mobile_number:res.data.data.mobile_number,
          email:res.data.data.email,
          access_level:res.data.data.access_level,
          team_lead:res.data.data.team_lead,
          username:res.data.data.username,
          communication_address:res.data.data.communication_address,
          location:res.data.data.location,
        })
      })
      .catch((err) => {
        console.log('details error = ', err.response)
      });
  };

  /**
   * Handle bank validation
   */
  handleUpdateEmployeeValidation = () => {
    const handleErrors = {};
    const {
      first_name,
      last_name,
      branch_id,
      status,
      role_id,
      mobile_number,
      email,
      access_level,
      team_lead,
      username,
      password,
      confirm_password,
      communication_address,
      location,
    } = this.state;


    if (first_name.length < 1) {
      handleErrors.first_name = EMPLOYEE_FORM_VALIDATIONS.FIRST_NAME;
    } else if (last_name.length < 1) {
      handleErrors.last_name = EMPLOYEE_FORM_VALIDATIONS.LAST_NAME;
    } else if (branch_id.length < 1) {
      handleErrors.branch_id = EMPLOYEE_FORM_VALIDATIONS.BRANCH_id;
    } else if (status.length < 1) {
      handleErrors.status = EMPLOYEE_FORM_VALIDATIONS.STATUS;
    } else if (role_id.length < 1) {
      handleErrors.role_id = EMPLOYEE_FORM_VALIDATIONS.ROLE;
    } else if (mobile_number.length < 1) {
      handleErrors.mobile_number = EMPLOYEE_FORM_VALIDATIONS.MO_NUMBER;
    } else if (email.length < 1) {
      handleErrors.email = EMPLOYEE_FORM_VALIDATIONS.EMAIL_ADDRESS;
    } else if (username.length < 1) {
      handleErrors.username = EMPLOYEE_FORM_VALIDATIONS.USERNAME;
    } else if (password != confirm_password) {
      handleErrors.confirm_password =
        EMPLOYEE_FORM_VALIDATIONS.CONFIRM_PASS_LENGTH;
    }
    this.setState({ handleErrors });
    return handleErrors;
  };

  /**
   * Handle Bank inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  /**
   * Handle add bank with validations
   */
  handleUpdateEmployee = () => {
    const errors = this.handleUpdateEmployeeValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.updateEmployee();
    }
  };

  updateEmployee = () => {
    this.setState({ loading : true })
    const {
      first_name,
      last_name,
      branch_id,
      status,
      role_id,
      mobile_number,
      email,
      access_level,
      team_lead,
      username,
      password,
      confirm_password,
      id
    } = this.state;

    const payload = {
      employee: {
        first_name,
        last_name,
        branch_id,
        status,
        role_id,
        mobile_number,
        email,
        access_level,
        team_lead,
        username,
      },
      admin: {
        username,
        password,
      },
    };

    let _this = this;
    console.log("payload , ", payload);
    updateEmployee(id, payload).then(res => {
        console.log('res.data  = ', res.data)
        if (res.data && res.data.status == 200) {
            this.setState({
              successMesg: res.data.msg,
              loading: false,
              errorMsg: "",
            });

            setTimeout(function () {
              _this.setState({ successMesg: "" });
              console.log('push history = ', _this.props.history)
              _this.props.history.push('/employee');
            }, 4000);
           
          } else {
            console.log('res.data !200 = ', res.data)
            this.setState({
              errorMsg: res.data.msg,
              loading: false,
              successMesg: "",
            });

            setTimeout(function () {
              _this.setState({ errorMsg: "" });
            }, 4000);
          }
    }).catch(err => {
        console.log('error => ', err.response)
        this.setState({
            errorMsg: err.response ? err.response.data.msg : "",
            loading: false,
          });
          setTimeout(function () {
            _this.setState({ resetMsg: "" });
          }, 4000);
    })
  };

  render() {
    const {
      first_name,
      last_name,
      branch_id,
      status,
      role_id,
      mobile_number,
      email,
      access_level,
      team_lead,
      username,
      password,
      confirm_password,
      handleErrors,
      all_roles,
      all_branches,
      errorMsg,
      successMesg,
      statuses,
      communication_address,
      location,
      loading
    } = this.state;

    return (
      <Fragment>
        <EmployeeUpdate
          first_name={first_name}
          last_name={last_name}
          branch_id={branch_id}
          status={status}
          role_id={role_id}
          mobile_number={mobile_number}
          email={email}
          access_level={access_level}
          team_lead={team_lead}
          username={username}
          password={password}
          confirm_password={confirm_password}
          handleInputValues={this.handleInputValues}
          handleErrors={handleErrors}
          handleUpdateEmployee={this.handleUpdateEmployee}
          all_roles={all_roles}
          all_branches={all_branches}
          successMesg={successMesg}
          errorMsg={errorMsg}
          statuses={statuses}
          communication_address={communication_address}
          location={location}
          loading={loading}
        />
      </Fragment>
    );
  }
}



export function mapStateToProps(state) {
  return {
    roles: state.FetchRolesReducer.roles,
    branches: state.fetchBranchesReducer.branches,
    statuses: state.FetchStatusReducer.statuses,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    getRoles: () => dispatch(getRoles()),
    getBranches: () => dispatch(getBranches()),
    getStatues: () => dispatch(getStatues()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeDetails);