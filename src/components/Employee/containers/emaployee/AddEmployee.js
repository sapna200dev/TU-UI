import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { AddEmployeeModal } from "../../components/employee";
import { EMPLOYEE_STATE, EMPLOYEE_FORM_VALIDATIONS } from "../../constant";
import { addEmployee } from "../../services/common";
import { getStatues, getBranches, getRoles } from "../../../Comman/actions";

class AddEmployee extends Component {
  state = EMPLOYEE_STATE;

  componentDidMount() {

    this.props.getRoles();
    this.props.getBranches();
    this.props.getStatues();
  }

  componentDidUpdate(prevProps) {
    const { branches, roles, statuses } = this.props;

    if (prevProps.roles != roles) {
      this.setState({ all_roles: roles });
    }
    if (prevProps.branches != branches) {
      this.setState({ all_branches: branches });
    }

    if (prevProps.statuses != statuses) {
      this.setState({ statuses });
    }
  }

  /**
   * Handle bank validation
   */
  handleAddEmployeeValidation = () => {
    const handleErrors = {};
    const {
      first_name,
      last_name,
      branch_id,
      status,
      role_id,
      mobile_number,
      email,
      access_level,
      team_lead,
      username,
      password,
      confirm_password,
      communication_address,
      location,
    } = this.state;

    if (first_name.length < 1) {
      handleErrors.first_name = EMPLOYEE_FORM_VALIDATIONS.FIRST_NAME;
    }  else if (!/^[a-zA-Z]*$/g.test(first_name)) {
      handleErrors.first_name = EMPLOYEE_FORM_VALIDATIONS.FIRST_NAME_VALID;
    } else if (last_name.length < 1) {
      handleErrors.last_name = EMPLOYEE_FORM_VALIDATIONS.LAST_NAME;
    } else if (!/^[a-zA-Z]*$/g.test(last_name)) {
      handleErrors.last_name = EMPLOYEE_FORM_VALIDATIONS.LAST_NAME_VALID;
    } else if (branch_id.length < 1) {
      handleErrors.branch_id = EMPLOYEE_FORM_VALIDATIONS.BRANCH_id;
    } else if (status.length < 1) {
      handleErrors.status = EMPLOYEE_FORM_VALIDATIONS.STATUS;
    } else if (role_id.length < 1) {
      handleErrors.role_id = EMPLOYEE_FORM_VALIDATIONS.ROLE;
    } else if (mobile_number.length < 1) {
      handleErrors.mobile_number = EMPLOYEE_FORM_VALIDATIONS.MO_NUMBER;
    } else if (mobile_number.length != 10) {
      handleErrors.mobile_number = EMPLOYEE_FORM_VALIDATIONS.MO_NUMBER_VALID;
    } else if (email.length < 1) {
      handleErrors.email = EMPLOYEE_FORM_VALIDATIONS.EMAIL_ADDRESS;
    } else if (communication_address.length < 1) {
      handleErrors.communication_address =
        EMPLOYEE_FORM_VALIDATIONS.COMMUNICATION_ADDRESS;
    } else if (location.length < 1) {
      handleErrors.location = EMPLOYEE_FORM_VALIDATIONS.LOCATION;
    } else if (username.length < 1) {
      handleErrors.username = EMPLOYEE_FORM_VALIDATIONS.USERNAME;
    } else if (password.length < 1) {
      handleErrors.password = EMPLOYEE_FORM_VALIDATIONS.PASSWORD;
    } else if (confirm_password.length < 1) {
      handleErrors.confirm_password = EMPLOYEE_FORM_VALIDATIONS.CONFIRM_PASS;
    } else if (password != confirm_password) {
      handleErrors.confirm_password =
        EMPLOYEE_FORM_VALIDATIONS.CONFIRM_PASS_LENGTH;
    }
    this.setState({ handleErrors });
    return handleErrors;
  };

  /**
   * Handle Bank inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  /**
   * Handle add bank with validations
   */
  handleAddEmployee = () => {
    const errors = this.handleAddEmployeeValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.addNewEmployee();
    }
  };

   /**
   * Handle add employee
   */ 
  addNewEmployee = () => {
    this.setState({ loading : true })
    const {
      first_name,
      last_name,
      branch_id,
      status,
      role_id,
      mobile_number,
      email,
      access_level,
      team_lead,
      username,
      password,
      confirm_password,
      communication_address,
      location,
    } = this.state;

    const payload = {
      employee: {
        first_name,
        last_name,
        branch_id,
        status,
        role_id,
        mobile_number,
        email,
        access_level,
        team_lead,
        username,
        communication_address,
        location,
      },
      admin: {
        username,
        password,
      },
    };

    let _this = this;
    addEmployee(payload)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMesg: res.data.msg,
            loading: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ successMesg: "" });
            _this.props.history.push('/employee');
          }, 4000);
        } else {

          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMesg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        console.log("error => ", err.response);
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
        setTimeout(function () {
          _this.setState({ resetMsg: "" });
        }, 4000);
      });
  };

  /**
   *  Handle reset event
   */
  handleResetEvent = () => {
    this.setState({
      first_name:'',
      last_name:'',
      branch_id:'',
      status:'',
      role_id:'',
      mobile_number:'',
      email:'',
      access_level:'',
      team_lead:'',
      username:'',
      password:'',
      confirm_password:'',
      communication_address:'',
      location:'',
    })
  }
  render() {
    const {
      first_name,
      last_name,
      branch_id,
      status,
      role_id,
      mobile_number,
      email,
      access_level,
      team_lead,
      username,
      password,
      confirm_password,
      handleErrors,
      all_roles,
      all_branches,
      errorMsg,
      successMesg,
      communication_address,
      location,
      statuses,
      loading
    } = this.state;

    return (
      <Fragment>
        <AddEmployeeModal
          first_name={first_name}
          last_name={last_name}
          branch_id={branch_id}
          status={status}
          role_id={role_id}
          mobile_number={mobile_number}
          email={email}
          access_level={access_level}
          team_lead={team_lead}
          username={username}
          password={password}
          confirm_password={confirm_password}
          communication_address={communication_address}
          location={location}
          handleInputValues={this.handleInputValues}
          handleErrors={handleErrors}
          handleAddEmployee={this.handleAddEmployee}
          all_roles={all_roles}
          all_branches={all_branches}
          successMesg={successMesg}
          errorMsg={errorMsg}
          statuses={statuses}
          loading={loading}
          handleResetEvent={this.handleResetEvent}
        />
      </Fragment>
    );
  }
}

export function mapStateToProps(state) {
  return {
    roles: state.FetchRolesReducer.roles,
    branches: state.fetchBranchesReducer.branches,
    statuses: state.FetchStatusReducer.statuses,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    getRoles: () => dispatch(getRoles()),
    getBranches: () => dispatch(getBranches()),
    getStatues: () => dispatch(getStatues()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEmployee);
