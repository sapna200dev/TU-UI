export const EMPLOYEE_STATE = {
    first_name:'',
    last_name:'',
    branch_id:'',
    status:'',
    role_id:'',
    mobile_number:'',
    communication_address:'',
    location:'',
    email:'',
    access_level:'',
    team_lead:'',
    username:'',
    password:'',
    confirm_password:'',
    handleErrors:{},
    all_roles:[],
    all_branches:[],
    successMesg:'',
    errorMsg:'',
    id:'',
    statuses:[],

};


export const EMPLOYEE_FORM_VALIDATIONS = {
    FIRST_NAME:'First name is required',
    LAST_NAME:'Last name is required',
    BRANCH_NAME:'Branch name is required',
    STATUS:'Status is required',
    ROLE:'Role is required ',
    MO_NUMBER : 'Mobile number is required',
    EMAIL_ADDRESS : 'Email Address is required',
    USERNAME : 'Username is required',
    PASSWORD:'Password is required',
    CONFIRM_PASS : 'Confirm password is required',
    CONFIRM_PASS_LENGTH : 'COnfirm password must match with password',
    LOCATION:'Location is required',
    COMMUNICATION_ADDRESS : 'Communication adddress is required',
    FIRST_NAME_VALID:'Only letters are allowd',
    LAST_NAME_VALID : 'Only letter are allowd',
    MO_NUMBER_VALID:'Number must be 10 digits only'
}