import React, { Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
} from "@coreui/react";
import { ContentLoading } from "../../../Comman/components";
import "../../containers/emaployee/employee.css";

const getBadge = (status) => {
  switch (status) {
    case "Active":
      return "success";
    case "Inactive":
      return "secondary";
    case "Pending":
      return "warning";
    case "Banned":
      return "danger";
    default:
      return "primary";
  }
};
const fields = ["name", "branch_name", "username", "role", "status", "Action"];

export const EmployeeList = (props) => {
  const {
    employeeLists,
    handleDeleteItem,
    errorMsg,
    successMsg,
    loading,
    handleFilterChange,
  } = props;

  console.log("employeeLists = ", employeeLists);

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> Employee List</b>

          <span>
            <div className="md-form mt-3">
              <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              />
              <a href="#/employee-add" className="button_style">
                <CButton
                  type="submit"
                  size="sm"
                  color="primary"
                  className="add_employee_button"
                  // onClick={() => handleDeleteItem(item)}
                >
                  {" "}
                  Add New
                </CButton>
              </a>
            </div>
          </span>
        </CCardHeader>

        <CCardBody>
          <CDataTable
            items={employeeLists}
            fields={fields}
            itemsPerPage={5}
            loading={loading}
            sort={true}
            pagination
            scopedSlots={{
              status: (item) => (
                <td>
                  {item.status}
                  {/* <CBadge color="success">
                        {item.status}
                      </CBadge> */}
                </td>
              ),
              Action: (item) => (
                <td>
                  <a
                    href={`#/employee-edit/${item.id}`}
                    className="button_style"
                  >
                    <CButton type="submit" size="sm" color="primary">
                      Edit {loading && <ContentLoading />}
                    </CButton>
                  </a>

                  <CButton
                    type="submit"
                    size="sm"
                    color="danger"
                    className="remove_button"
                    onClick={() => handleDeleteItem(item)}
                  >
                    Delete
                  </CButton>
                  <a
                    href={`#/employee-view/${item.id}`}
                    className="button_style"
                  >
                    <CButton
                      type="submit"
                      size="sm"
                      color="primary"
                      className="remove_button"
                    >
                      View
                    </CButton>
                  </a>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>
    </Fragment>
  );
};
