import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

export const ViewEmployeeModal = (props) => {
  const {
    first_name,
    last_name,
    branch_name,
    status,
    role,
    mobile_number,
    email,
    access_level,
    team_lead,
    username,
    password,
    confirm_password,
    handleInputValues,
    handleErrors,
    handleUpdateEmployee,
    all_branches,
    all_roles,
    successMesg,
    errorMsg,
  } = props;

  let arrayUniqueRoles = [];

  if (all_roles) {
    arrayUniqueRoles = [
      ...new Map(all_roles.map((item) => [item["name"], item])).values(),
    ];
  }

  console.log("arrayUniqueRolesarrayUniqueRoles = ", arrayUniqueRoles);

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b> Employee Details</b>
          </h4>
        </CCardHeader>

        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="first_name">
                  First Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="first_name"
                  name="first_name"
                  value={first_name}
                  placeholder="First Name"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Last Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="last_name"
                  name="last_name"
                  value={last_name}
                  placeholder="Last Name "
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="branch_name">
                  Branch <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="branch_name"
                  id="branch_name"
                  value={branch_name}
                  disabled
                >
                  <option value="">Please select</option>
                  {all_branches.length > 0 ? (
                    all_branches.map((item) => (
                      <option value={item.id}>{item.branch_name}</option>
                    ))
                  ) : (
                    <option value="">No Branches</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="status">
                  Status <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="status"
                  id="status"
                  value={status}
                  disabled
                >
                  <option value="">Please select</option>
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="role">
                  Roles <small className="required_lable">*</small>
                </CLabel>

                <CSelect custom name="role" id="role" value={role} disabled>
                  <option value="">Please select</option>
                  {arrayUniqueRoles.map((item) => (
                    <option
                      value={item.id}
                      selected={item.id === role ? "selected" : ""}
                    >
                      {item.display_name}
                    </option>
                  ))}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="mobile_number">
                  Mobile Number <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="mobile_number"
                  name="mobile_number"
                  value={mobile_number}
                  placeholder=" Mobile Number"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="email">
                  Email Address <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="email"
                  name="email"
                  value={email}
                  placeholder="Email Address"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="access_level">
                  Access Level <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="access_level"
                  name="access_level"
                  placeholder=" Access Level"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="team_lead">
                  Team Lead <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="team_lead"
                  name="team_lead"
                  placeholder=" Team Lead"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CCardFooter className="divider_layout"></CCardFooter>
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="username">
                  Username <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="username"
                  name="username"
                  value={username}
                  placeholder="Username"
                  disabled
                />
              </CCol>
              {/* <CCol xs="12" md="3">
                <CLabel htmlFor="password">
                  Password <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  placeholder="Password"
                  disabled
                />
              </CCol> */}
              {/* <CCol xs="12" md="3">
                <CLabel htmlFor="confirm_password">
                  Confirm Password<small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="confirm_password"
                  name="confirm_password"
                  value={confirm_password}
                  placeholder="Confirm Password"
                />
              </CCol> */}
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>
        <CCardFooter>
          <a href="#/employee" style={{ color: "white" }}>
            <CButton type="submit" size="sm" color="primary">
              <CIcon name="cil-scrubber" /> Go Back
            </CButton>
          </a>
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
