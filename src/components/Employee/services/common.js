import axios from "axios";
import {URL_HOST} from '../../Comman/constants'


export const getRoles = () =>
axios.get(`${URL_HOST}/api/role/all`);


export const getBranches = (bank_id) =>
axios.get(`${URL_HOST}/api/branches/all/${bank_id}`);


export const updateEmployee = (id, data) => axios.post(`${URL_HOST}/api/employee/update/${id}`, data);

export const deleteEmployee = (id, data) => axios.post(`${URL_HOST}/api/employee/delete/${id}`, data);

export const addEmployee = (data) =>
axios.post(`${URL_HOST}/api/employee/create`, data);

export const employeeLists = () =>  axios.get(`${URL_HOST}/api/employee/list`);

export const getEmployeeDetails = (id) =>  axios.get(`${URL_HOST}/api/employee/${id}`);