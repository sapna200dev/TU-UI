export {
    getBranches,
    getRoles,
    addEmployee,
    employeeLists,
    getEmployeeDetails,
    updateEmployee,
    deleteEmployee
   } from '/common';