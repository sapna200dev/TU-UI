import { BANK_FORM_VALIDATIONS } from "./constant";

export const ValidateRequestParams = (params) => {
  const addBankErrors = {};
  const {
    bank_name,
    address,
    location,
    status,
    billing_status,
    company_email,
    company_phone,
    contact_name,
    contact_phone,
    contact_email,
    contact_location,
    billing_cycle,
    billing_cycle_from,
    billing_cycle_to,
    confirm_password,
    password,
    admin_username,
    upload_1,
    upload_2,
    upload_3,
    upload_4,
    upload_5,
  } = params;

  if (bank_name.length < 1) {
    addBankErrors.bank_name = BANK_FORM_VALIDATIONS.BANK_NAME;
  } else if (!/^[a-zA-Z]*$/g.test(bank_name)) {
    addBankErrors.bank_name = BANK_FORM_VALIDATIONS.BANK_NAME_VALID;
  } else if (location.length < 1) {
    addBankErrors.location = BANK_FORM_VALIDATIONS.LOCATION;
  } else if (address.length < 1) {
    addBankErrors.address = BANK_FORM_VALIDATIONS.ADDRESS;
  } else if (status.length < 1) {
    addBankErrors.status = BANK_FORM_VALIDATIONS.STATUS;
  } else if (billing_status.length < 1) {
    addBankErrors.billing_status = BANK_FORM_VALIDATIONS.BILLING_STATUS;
  } else if (company_email.length < 1) {
    addBankErrors.company_email = BANK_FORM_VALIDATIONS.COMPANY_EMAIL;
  } else if (!/\S+@\S+\.\S+/.test(company_email)) {
    addBankErrors.company_email = BANK_FORM_VALIDATIONS.VALID_EMAIL;
  } else if (company_phone.length < 1) {
    addBankErrors.company_phone = BANK_FORM_VALIDATIONS.COMPANY_PHONE;
  } else if (company_phone.length != 10) {
    addBankErrors.company_phone =
      BANK_FORM_VALIDATIONS.PHONE_NUMBER_VALIDATE_MSG;
  } else if (contact_name.length < 1) {
    addBankErrors.contact_name = BANK_FORM_VALIDATIONS.CONTACT_NAME;
  } else if (contact_phone.length < 1) {
    addBankErrors.contact_phone = BANK_FORM_VALIDATIONS.CONTACT_PHONE;
  } else if (contact_phone.length != 10) {
    addBankErrors.contact_phone =
      BANK_FORM_VALIDATIONS.PHONE_NUMBER_VALIDATE_MSG;
  } else if (contact_email.length < 1) {
    addBankErrors.contact_email = BANK_FORM_VALIDATIONS.CONTACT_EMAIL;
  } else if (!/\S+@\S+\.\S+/.test(contact_email)) {
    addBankErrors.contact_email = BANK_FORM_VALIDATIONS.VALID_EMAIL;
  } else if (contact_location.length < 1) {
    addBankErrors.contact_location = BANK_FORM_VALIDATIONS.CONTACT_LOCATION;
  } else if (billing_cycle.length < 1) {
    addBankErrors.billing_cycle = BANK_FORM_VALIDATIONS.BILLING_CYCLE;
  } else if (billing_cycle_from.length < 1) {
    addBankErrors.billing_cycle_from = BANK_FORM_VALIDATIONS.BILLING_CYCLE_FROM;
  } else if (billing_cycle_to.length < 1) {
    addBankErrors.billing_cycle_to = BANK_FORM_VALIDATIONS.BILLING_CYCLE_TO;
  } else if (upload_1 === "") {
    addBankErrors.upload_1 = BANK_FORM_VALIDATIONS.UPLOAD_1;
  } else if (upload_2 === "") {
    addBankErrors.upload_2 = BANK_FORM_VALIDATIONS.UPLOAD_2;
  } else if (upload_3 === "") {
    addBankErrors.upload_3 = BANK_FORM_VALIDATIONS.UPLOAD_3;
  } else if (upload_4 === "") {
    addBankErrors.upload_4 = BANK_FORM_VALIDATIONS.UPLOAD_4;
  } else if (upload_5 === "") {
    addBankErrors.upload_5 = BANK_FORM_VALIDATIONS.UPLOAD_5;
  } else if (admin_username.length < 1) {
    addBankErrors.admin_username = BANK_FORM_VALIDATIONS.ADMIN_USERNAME;
  } else if (password.length < 1) {
    addBankErrors.password = BANK_FORM_VALIDATIONS.PASSWORD;
  } else if (confirm_password.length < 1) {
    addBankErrors.confirm_password = BANK_FORM_VALIDATIONS.CONFIRM_PASS;
  } else if (password != confirm_password) {
    addBankErrors.confirm_password = BANK_FORM_VALIDATIONS.CONFIRM_PASS_LENGTH;
  }

  return addBankErrors;
};

/**
 * Handle request
 * @param  props
 */
export const validateUpdateRequest = (props) => {
  const addBankErrors = {};
  const {
    bank_name,
    address,
    location,
    status,
    billing_status,
    company_email,
    company_phone,
    contact_name,
    contact_phone,
    contact_email,
    contact_location,
    billing_cycle,
    billing_cycle_from,
    billing_cycle_to,
    confirm_password,
    password,
    admin_username,
  } = props;

  if (bank_name.length < 1) {
    addBankErrors.bank_name = BANK_FORM_VALIDATIONS.BANK_NAME;
  } else if (location.length < 1) {
    addBankErrors.location = BANK_FORM_VALIDATIONS.LOCATION;
  } else if (address.length < 1) {
    addBankErrors.address = BANK_FORM_VALIDATIONS.ADDRESS;
  } else if (status.length < 1) {
    addBankErrors.status = BANK_FORM_VALIDATIONS.STATUS;
  } else if (billing_status.length < 1) {
    addBankErrors.billing_status = BANK_FORM_VALIDATIONS.BILLING_STATUS;
  } else if (company_email.length < 1) {
    addBankErrors.company_email = BANK_FORM_VALIDATIONS.COMPANY_EMAIL;
  } else if (company_phone.length < 1) {
    addBankErrors.company_phone = BANK_FORM_VALIDATIONS.COMPANY_PHONE;
  } else if (contact_name.length < 1) {
    addBankErrors.contact_name = BANK_FORM_VALIDATIONS.CONTACT_NAME;
  } else if (contact_phone.length < 1) {
    addBankErrors.contact_phone = BANK_FORM_VALIDATIONS.CONTACT_PHONE;
  } else if (contact_email.length < 1) {
    addBankErrors.contact_email = BANK_FORM_VALIDATIONS.CONTACT_EMAIL;
  } else if (!/\S+@\S+\.\S+/.test(contact_email)) {
    addBankErrors.contact_email = BANK_FORM_VALIDATIONS.CONTACT_EMAIL_VALID;
  } else if (contact_location.length < 1) {
    addBankErrors.contact_location = BANK_FORM_VALIDATIONS.CONTACT_LOCATION;
  } else if (billing_cycle.length < 1) {
    addBankErrors.billing_cycle = BANK_FORM_VALIDATIONS.BILLING_CYCLE;
  } else if (billing_cycle_from.length < 1) {
    addBankErrors.billing_cycle_from = BANK_FORM_VALIDATIONS.BILLING_CYCLE_FROM;
  } else if (billing_cycle_to.length < 1) {
    addBankErrors.billing_cycle_to = BANK_FORM_VALIDATIONS.BILLING_CYCLE_TO;
  } else if (admin_username.length < 1) {
    addBankErrors.admin_username = BANK_FORM_VALIDATIONS.ADMIN_USERNAME;
  } else if (password != confirm_password) {
    addBankErrors.confirm_password = BANK_FORM_VALIDATIONS.CONFIRM_PASS_LENGTH;
  }

  return addBankErrors;
};


/**
 * Makde form request params
 * @param  props 
 */
export const prepareFormRequestParams = (props) => {
  const {
    bank_name,
    address,
    location,
    status,
    billing_status,
    company_email,
    contact_email,
    company_phone,
    contact_name,
    contact_phone,
    contact_location,
    billing_cycle,
    billing_cycle_from,
    billing_cycle_to,
    password,
    admin_username,
    upload_1,
    upload_2,
    upload_3,
    upload_4,
    upload_5,
  } = props;

  var formDataRequest = new FormData();

  if (upload_1) { formDataRequest.append("upload_1", upload_1); }

  if (upload_2) { formDataRequest.append("upload_2", upload_2); }

  if (upload_3) { formDataRequest.append("upload_3", upload_3); }

  if (upload_4) { formDataRequest.append("upload_4", upload_4); }

  if (upload_4) { formDataRequest.append("upload_5", upload_5); }

  formDataRequest.append("bank_name", bank_name);
  formDataRequest.append("address", address);
  formDataRequest.append("location", location);
  formDataRequest.append("status", status);
  formDataRequest.append("billing_status", billing_status);
  formDataRequest.append("company_email", company_email);
  formDataRequest.append("contact_email", contact_email);
  formDataRequest.append("company_phone", company_phone);
  formDataRequest.append("contact_name", contact_name);
  formDataRequest.append("contact_phone", contact_phone);
  formDataRequest.append("contact_location", contact_location);
  formDataRequest.append("billing_cycle", billing_cycle);
  formDataRequest.append("billing_cycle_from", billing_cycle_from);
  formDataRequest.append("billing_cycle_to", billing_cycle_to);
  formDataRequest.append("admin_username", admin_username);

  formDataRequest.append("password", password);

  return formDataRequest;
};
