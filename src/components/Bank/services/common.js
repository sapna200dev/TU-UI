import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'


export const addBank = (data) =>
axios.post(`${URL_HOST}/api/bank/add`, data);


export const bankLists = () =>
axios.get(`${URL_HOST}/api/bank/all`);

export const deleteBank = (id) =>
axios.post(`${URL_HOST}/api/bank/delete/${id}`);

export const getBankRecord = (id) =>
axios.get(`${URL_HOST}/api/bank/${id}`);

export const updateBank = (id, data) =>
axios.post(`${URL_HOST}/api/bank/update/${id}`, data);
