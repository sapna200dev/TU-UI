import React, { Component, Fragment } from "react";
import { ViewBank } from "../components/Bank";
import { getBankRecord } from "../services";
import { BANK_STATE } from "../constant";

export class ShowBank extends Component {
  state = BANK_STATE;

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({
      id
    })
    this.getBankDetails(id);

    console.log("bank detailsdddddd");
  }

  getBankDetails = (id) => {
    getBankRecord(id)
      .then((res) => {


        if (res.data && res.data.status == 200) {
       
          this.setState({
            bankDetail: res.data.data,
            loading: false,
            errorMsg: "",
            bank_name: res.data.data.bank_name,
            address: res.data.data.address,
            location:res.data.data.location,
            status:res.data.data.status,
            billing_status:res.data.data.billing_status,
            company_email:res.data.data.company_email,
            contact_email:res.data.data.contact_email,
            company_phone:res.data.data.company_phone,
            contact_name:res.data.data.contact_name,
            contact_phone:res.data.data.contact_phone,
            contact_location:res.data.data.contact_location,
            billing_cycle:res.data.data.billing_cycle,
            billing_cycle_from:res.data.data.billing_cycle_from,
            billing_cycle_to:res.data.data.billing_cycle_to,
            admin_username:res.data.data.admin_username,
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
          });
        }
        console.log("bankDetail => ", res);
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
      });
  };






  render() {
    const {
      bankDetail,
      bank_name,
      address,
      location,
      status,
      billing_status,
      company_email,
      company_phone,
      contact_name,
      contact_phone,
      contact_location,
      billing_cycle,
      billing_cycle_from,
      billing_cycle_to,
      confirm_password,
      password,
      admin_username,
      upload_1,
      upload_2,
      upload_3,
      upload_4,
      upload_5,
    } = this.state;

    return (
      <Fragment>
        <div>
          <ViewBank
            bankDetail={bankDetail}
            bank_name={bank_name}
            location={location}
            status={status}
            billing_status={billing_status}
            company_email={company_email}
            company_phone={company_phone}
            contact_name={contact_name}
            contact_phone={contact_phone}
            contact_location={contact_location}
            billing_cycle={billing_cycle}
            billing_cycle_from={billing_cycle_from}
            billing_cycle_to={billing_cycle_to}
            password={password}
            confirm_password={confirm_password}
            admin_username={admin_username}
            upload_1={upload_1}
            upload_2={upload_2}
            upload_3={upload_3}
            upload_4={upload_4}
            upload_5={upload_5}
            address={address}
          />
        </div>
      </Fragment>
    );
  }
}
