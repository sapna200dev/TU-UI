import React, { Component, Fragment } from "react";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from "@coreui/react";
import { BankList } from "../components/Bank";
import { bankLists, deleteBank } from "../services";
import { BANK_STATE, BANK_FORM_VALIDATIONS } from "../constant";

export class Banks extends Component {
  state = {
    bankList: [],
    isDelete: false,
    deleteItem: "",
    successMsg: "",
    loading: false,
    errorMsg: "",
    searchValue: "",
    filterBankLists: [],
  };

  componentDidMount() {
    this.getAllBanks();
  }

  getAllBanks = () => {
    bankLists()
      .then((res) => {
        console.log("bank list ", res);
        this.setState({
          bankList: res.data,
          filterBankLists: res.data,
        });
      })
      .catch((err) => {
        console.log("bank list  error", err);
      });
  };

  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  handleDeleteConfirm = () => {
    const ids = [];
    const { deleteItem } = this.state;

    deleteBank(deleteItem.id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
          });
          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
          });
        }

        this.getAllBanks();
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
      });
  };

  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { bankList } = this.state;
    let arrayBankLists = [];

    if (data) {
      arrayBankLists = this.handleFilterChangeVal(bankList, data);
    } else {
      arrayBankLists = this.state.bankList;
    }

    this.setState({
      filterBankLists: arrayBankLists,
      searchValue: data,
    });
  };

  /**
   * Handle search event
   */
  handleFilterChangeVal = (bankLists, value) => {
    let bankList = [];
    bankList = bankLists.filter((item) => {
      return (
        (item.bank_name &&
          item.bank_name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.location &&
          item.location.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.address &&
          item.address.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.billing_status &&
          item.billing_status.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.billing_cycle &&
          item.billing_cycle.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.status &&
          item.status.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return bankList;
  };

  render() {
    const {
      filterBankLists,
      isDelete,
      deleteItem,
      errorMsg,
      successMsg,
      loading,
      searchValue,
    } = this.state;

    return (
      <Fragment>
        <div>
          <BankList
            bankList={filterBankLists}
            errorMsg={errorMsg}
            successMsg={successMsg}
            loading={loading}
            handleDeleteItem={this.handleDeleteItem}
            searchValue={searchValue}
            handleFilterChange={this.handleFilterChange}
          />

          {isDelete && (
            <CModal
              show={isDelete}
              onClose={this.handleCancelModal}
              color="danger"
            >
              <CModalHeader closeButton>
                <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
              </CModalHeader>
              <CModalBody>
                Are you sure to delete this Bank <b>{deleteItem.name}</b>
              </CModalBody>
              <CModalFooter>
                <CButton color="danger" onClick={this.handleDeleteConfirm}>
                  Delete
                </CButton>{" "}
                <CButton color="secondary" onClick={this.handleCancelModal}>
                  Cancel
                </CButton>
              </CModalFooter>
            </CModal>
          )}
        </div>
      </Fragment>
    );
  }
}
