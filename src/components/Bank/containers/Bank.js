import React, { Component, Fragment } from "react";
import "./bankStyle.css";
import { AddBank } from "../components/Bank";
import { addBank } from "../services";
import { BANK_STATE } from "../constant";
import { ValidateRequestParams, prepareFormRequestParams } from "../functions";

export class Bank extends Component {
  state = BANK_STATE;

  /**
   * Handle bank validation
   */
  handleAddBankValidation = () => { 
    // Request validate
    let addBankErrors = ValidateRequestParams(this.state);

    this.setState({ addBankErrors });
    return addBankErrors;
  };

  /**
   * Handle add bank with validations
   */
  handleAddBank = () => {
    const errors = this.handleAddBankValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.addNewBank();
    }
  };

  /**
   * Handle add bank
   */
  addNewBank = () => {
    this.setState({ loading: true });

    // Make form request payload
    let formRequest = prepareFormRequestParams(this.state);

    let _this = this;
    addBank(formRequest) 
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            resetMsg: res.data.msg,
            loading: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ resetMsg: "" });
            _this.props.history.push("/bank-list");
          }, 4000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            resetMsg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
        setTimeout(function () {
          _this.setState({ resetMsg: "" });
        }, 4000);
      });
  };

  /**
   * Handle Bank inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleInputValuesUpload2 = (event, uploadType = null) => {
    if (uploadType == "coi_upload") {
      this.setState({
        upload_1: event.target.files[0],
      });
    } else if (uploadType == "gst_upload") {
      this.setState({
        upload_2: event.target.files[0],
      });
    } else if (uploadType == "pan_upload") {
      this.setState({
        upload_3: event.target.files[0],
      });
    } else if (uploadType == "agreement_upload") {
      this.setState({
        upload_4: event.target.files[0],
      });
    } else if (uploadType == "customer_logo_upload") {
      this.setState({
        upload_5: event.target.files[0],
      });
    }
  };

  /**
   * Handle reset form event
   */
  handleResetFormEvent = () => {
    this.setState({
      bank_name: "",
      address: "",
      location: "",
      status: "",
      billing_status: "",
      company_email: "",
      contact_email: "",
      company_phone: "",
      contact_name: "",
      contact_phone: "",
      contact_location: "",
      billing_cycle: "",
      billing_cycle_from: "",
      billing_cycle_to: "",
      password: "",
      admin_username: "",
      upload_1: "",
      upload_2: "",
      upload_3: "",
      upload_4: "",
      upload_5: "",
    });
  };

  render() {
    const {
      addBankErrors,
      resetMsg,
      errorMsg,

      bank_name,
      address,
      location,
      status,
      billing_status,
      company_email,
      company_phone,
      contact_name,
      contact_phone,
      contact_location,
      billing_cycle,
      billing_cycle_from,
      billing_cycle_to,
      confirm_password,
      password,
      admin_username,
      upload_1,
      upload_2,
      upload_3,
      upload_4,
      upload_5,
      loading,
    } = this.state;

    return (
      <Fragment>
        <div>
          <AddBank
            bank_name={bank_name}
            location={location}
            status={status}
            billing_status={billing_status}
            company_email={company_email}
            company_phone={company_phone}
            contact_name={contact_name}
            contact_phone={contact_phone}
            contact_location={contact_location}
            billing_cycle={billing_cycle}
            billing_cycle_from={billing_cycle_from}
            billing_cycle_to={billing_cycle_to}
            password={password}
            confirm_password={confirm_password}
            admin_username={admin_username}
            upload_1={upload_1}
            upload_2={upload_2}
            upload_3={upload_3}
            upload_4={upload_4}
            upload_5={upload_5}
            address={address}
            addBankErrors={addBankErrors}
            resetMsg={resetMsg}
            errorMsg={errorMsg}
            loading={loading}
            handleInputValues={this.handleInputValues}
            handleAddBank={this.handleAddBank}
            handleInputValuesUpload2={this.handleInputValuesUpload2}
            handleResetFormEvent={this.handleResetFormEvent}
          />
        </div>
      </Fragment>
    );
  }
}
