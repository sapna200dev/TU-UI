import React, { Component, Fragment } from "react";
import "./bankStyle.css";
import { UpdateBank } from "../components/Bank";
import { getBankRecord, updateBank } from "../services";
import { BANK_STATE, BANK_FORM_VALIDATIONS } from "../constant";
import { validateUpdateRequest, prepareFormRequestParams } from "../functions";




export class BankDetail extends Component {
  state = BANK_STATE;

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({ id });

    // Get bank details by id
    this.getBankDetails(id);
  }

  /**
   * Get bank details
   */
  getBankDetails = (id) => {
    getBankRecord(id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            loading: false,
            errorMsg: "",
            bank_name: res.data.data.bank_name,
            address: res.data.data.address,
            location: res.data.data.location,
            status: res.data.data.status,
            billing_status: res.data.data.billing_status,
            company_email: res.data.data.company_email,
            contact_email: res.data.data.contact_email,
            company_phone: res.data.data.company_phone,
            contact_name: res.data.data.contact_name,
            contact_phone: res.data.data.contact_phone,
            contact_location: res.data.data.contact_location,
            billing_cycle: res.data.data.billing_cycle,
            billing_cycle_from: res.data.data.billing_cycle_from,
            billing_cycle_to: res.data.data.billing_cycle_to,
            admin_username: res.data.data.admin_username,
            uploaded_1: res.data.data.upload_1,
            uploaded_2: res.data.data.upload_2,
            uploaded_3: res.data.data.upload_3,
            uploaded_4: res.data.data.upload_4,
            uploaded_5: res.data.data.upload_5,
          });
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
          });
        }
      })
      .catch((err) => {
        console.log("error => ", err);
      });
  };

  /**
   * Handle Bank inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  /**
   * Handle bank validation
   */
  handleAddBankValidation = () => {
    // Request validate
    let addBankErrors = validateUpdateRequest(this.state);

    this.setState({ addBankErrors });
    return addBankErrors;
  };

  /**
   * Handle add bank with validations
   */
  handleUpdateBank = () => {
    const errors = this.handleAddBankValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.updateBankRecod();
    }
  };

  /**
   * Handle add bank
   */
  updateBankRecod = () => {
    this.setState({ loading: true });
    const { id } = this.state;

    // Make form request payload
    let formRequest = prepareFormRequestParams(this.state);

    let _this = this;
    updateBank(id, formRequest)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            succesMsg: res.data.msg,
            loading: false,
            errorMsg: "",
          });
          // _this.props.history.push("/bank-list");

          setTimeout(function () {
            _this.setState({ succesMsg: "" });
            _this.props.history.push("/bank-list");
          }, 4000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            succesMsg: "",
          });
          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
        setTimeout(function () {
          _this.setState({ succesMsg: "" });
        }, 4000);
      });
  };



  /**
   * Handle upload docs events
   */
  handleInputValuesUpload2 = (event, uploadType = null) => {
    if (uploadType == "coi_upload") {
      this.setState({
        upload_1: event.target.files[0],
      });
    } else if (uploadType == "gst_upload") {
      this.setState({
        upload_2: event.target.files[0],
      });
    } else if (uploadType == "pan_upload") {
      this.setState({
        upload_3: event.target.files[0],
      });
    } else if (uploadType == "agreement_upload") {
      this.setState({
        upload_4: event.target.files[0],
      });
    } else if (uploadType == "customer_logo_upload") {
      this.setState({
        upload_5: event.target.files[0],
      });
    }
  };

  render() {
    const {
      bankDetail,
      errorMsg,
      bank_name,
      address,
      location,
      status,
      billing_status,
      company_email,
      company_phone,
      contact_name,
      contact_phone,
      contact_location,
      billing_cycle,
      billing_cycle_from,
      billing_cycle_to,
      confirm_password,
      password,
      admin_username,
      upload_1,
      upload_2,
      upload_3,
      upload_4,
      upload_5,
      addBankErrors,
      succesMsg,
      loading,
      uploaded_1,
      uploaded_2,
      uploaded_3,
      uploaded_4,
      uploaded_5,
      contact_email,
    } = this.state;

    return (
      <Fragment>
        <div>
          <UpdateBank
            uploaded_1={uploaded_1}
            bankDetail={bankDetail}
            bank_name={bank_name}
            location={location}
            status={status}
            billing_status={billing_status}
            company_email={company_email}
            company_phone={company_phone}
            contact_name={contact_name}
            contact_phone={contact_phone}
            contact_location={contact_location}
            billing_cycle={billing_cycle}
            billing_cycle_from={billing_cycle_from}
            billing_cycle_to={billing_cycle_to}
            password={password}
            confirm_password={confirm_password}
            admin_username={admin_username}
            upload_1={upload_1}
            upload_2={upload_2}
            upload_3={upload_3}
            upload_4={upload_4}
            upload_5={upload_5}
            address={address}
            errorMsg={errorMsg}
            addBankErrors={addBankErrors}
            succesMsg={succesMsg}
            errorMsg={errorMsg}
            loading={loading}
            handleInputValues={this.handleInputValues}
            handleUpdateBank={this.handleUpdateBank}
            uploaded_2={uploaded_2}
            uploaded_3={uploaded_3}
            uploaded_4={uploaded_4}
            uploaded_5={uploaded_5}
            contact_email={contact_email}
            handleInputValuesUpload2={this.handleInputValuesUpload2}
          />
        </div>
      </Fragment>
    );
  }
}
