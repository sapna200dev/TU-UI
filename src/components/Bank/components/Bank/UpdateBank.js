import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "./AddBankStyle.css";
import { URL_HOST } from "../../../Comman/constants";
import { ContentLoading, ShowToastMessage } from "../../../Comman/components";

export const UpdateBank = (props) => {
  const {
    handleInputValues,
    handleAddBank,
    addBankErrors,
    succesMsg,
    errorMsg,
    bank_name,
    address,
    location,
    status,
    billing_status,
    company_email,
    contact_email,
    company_phone,
    contact_name,
    contact_phone,
    contact_location,
    billing_cycle,
    billing_cycle_from,
    billing_cycle_to,
    confirm_password,
    password,
    admin_username,
    upload_1,
    upload_2,
    upload_3,
    upload_4,
    upload_5,
    uploaded_1,
    uploaded_2,
    uploaded_3,
    uploaded_4,
    uploaded_5,
    handleUpdateBank,
    handleInputValuesUpload2,
    loading,
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b> Edit Bank</b>
          </h4>
        </CCardHeader>
        {succesMsg && (
          <CAlert color="success" className="msg_div">
            {succesMsg}
          </CAlert>
            // <ShowToastMessage
            //     success={true}
            //     header={'Success'}
            //     content={succesMsg}
            // />
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="bank_name">
                  Bank Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="bank_name"
                  name="bank_name"
                  value={bank_name}
                  placeholder="Bank Name"
                  onChange={handleInputValues}
                  invalid={addBankErrors.bank_name}
                />
                <CInvalidFeedback>{addBankErrors.bank_name}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="branch_name">
                  HO Location <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="location"
                  name="location"
                  value={location}
                  placeholder="HO Location"
                  onChange={handleInputValues}
                  invalid={addBankErrors.location}
                />
                <CInvalidFeedback>{addBankErrors.location}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="address">
                  Address <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="address"
                  name="address"
                  value={address}
                  placeholder="Common Address"
                  onChange={handleInputValues}
                  invalid={addBankErrors.address}
                />
                <CInvalidFeedback>{addBankErrors.address}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="status">
                  Status <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="status"
                  id="status"
                  invalid={addBankErrors.status}
                  value={status}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </CSelect>
                {/* <CInput
                  id="status"
                  name="status"
                  value={status}
                  placeholder="Status"
                  onChange={handleInputValues}
                  invalid={addBankErrors.status}
                /> */}
                <CInvalidFeedback>{addBankErrors.status}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="billing_status">
                  Billing Status <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="billing_status"
                  name="billing_status"
                  value={billing_status}
                  placeholder="Billing Status"
                  onChange={handleInputValues}
                  invalid={addBankErrors.billing_status}
                />
                <CInvalidFeedback>
                  {addBankErrors.billing_status}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="company_email">
                  Compnay Email <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="company_email"
                  name="company_email"
                  value={company_email}
                  placeholder="Compnay Email"
                  onChange={handleInputValues}
                  invalid={addBankErrors.company_email}
                />
                <CInvalidFeedback>
                  {addBankErrors.company_email}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="company_phone">
                  Company Phone <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="company_phone"
                  name="company_phone"
                  value={company_phone}
                  placeholder="Compnay Phone"
                  onChange={handleInputValues}
                  invalid={addBankErrors.company_phone}
                />
                <CInvalidFeedback>
                  {addBankErrors.company_phone}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="contact_name">
                  Contact Person <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="contact_name"
                  name="contact_name"
                  value={contact_name}
                  placeholder="Contact Person"
                  onChange={handleInputValues}
                  invalid={addBankErrors.contact_name}
                />
                <CInvalidFeedback>
                  {addBankErrors.contact_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="contact_phone">
                  Contact Person Phone{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="contact_phone"
                  name="contact_phone"
                  value={contact_phone}
                  placeholder="Contact Person Phone"
                  onChange={handleInputValues}
                  invalid={addBankErrors.contact_phone}
                />
                <CInvalidFeedback>
                  {addBankErrors.contact_phone}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="contact_location">
                  Contact Person Email{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="email"
                  id="contact_email"
                  name="contact_email"
                  value={contact_email}
                  placeholder="Contact Person Email"
                  onChange={handleInputValues}
                  invalid={addBankErrors.contact_email}
                />
                <CInvalidFeedback>
                  {addBankErrors.contact_email}
                </CInvalidFeedback>
              </CCol>

              <CCol md="3">
                <CLabel htmlFor="contact_location">
                  Contact Person Location{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="contact_location"
                  name="contact_location"
                  value={contact_location}
                  placeholder="Contact Person Location"
                  onChange={handleInputValues}
                  invalid={addBankErrors.contact_location}
                />
                <CInvalidFeedback>
                  {addBankErrors.contact_location}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="billing_cycle">
                  Billing Cycle <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="billing_cycle"
                  id="billing_cycle"
                  invalid={addBankErrors.billing_cycle}
                  value={billing_cycle}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  <option value="quarterly">Quarterly</option>
                  <option value="monthly">Monthly</option>
                  <option value="yearly">Yearly</option>
                </CSelect>
                {/* <CInput
                  id="billing_cycle"
                  name="billing_cycle"
                  value={billing_cycle}
                  placeholder="Billing Cycle"
                  onChange={handleInputValues}
                  invalid={addBankErrors.billing_cycle}
                /> */}
                <CInvalidFeedback>
                  {addBankErrors.billing_cycle}
                </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="billing_cycle_from">
                  Billing Cycle From <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="date"
                  id="billing_cycle_from"
                  name="billing_cycle_from"
                  value={billing_cycle_from}
                  placeholder="Billing Cycle From"
                  onChange={handleInputValues}
                  invalid={addBankErrors.billing_cycle_from}
                />
                <CInvalidFeedback>
                  {addBankErrors.billing_cycle_from}
                </CInvalidFeedback>
              </CCol>

              <CCol md="3">
                <CLabel htmlFor="billing_cycle_to">
                  Billing Cycle To <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="date"
                  id="billing_cycle_to"
                  name="billing_cycle_to"
                  value={billing_cycle_to}
                  placeholder="Billing Cycle To"
                  onChange={handleInputValues}
                  invalid={addBankErrors.billing_cycle_to}
                />
                <CInvalidFeedback>
                  {addBankErrors.billing_cycle_to}
                </CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_1" name="upload_1">
                  COI Upload <small className="required_lable">* </small>
                </CLabel>
                <CInputFile
                  id="upload_1"
                  name="upload_1"
                  // value={upload_1}
                  onChange={(e) => handleInputValuesUpload2(e, "coi_upload")}
                  invalid={addBankErrors.upload_1}
                />
                <a
                  href={`${URL_HOST}${uploaded_1}`}
                  target="_blank"
                  className="uploaded_files"
                >
                  {uploaded_1}
                </a>
                <CInvalidFeedback>{addBankErrors.upload_1}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_2">
                  GST Upload <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_2"
                  name="upload_2"
                  // value={upload_2}
                  onChange={(e) => handleInputValuesUpload2(e, "gst_upload")}
                  invalid={addBankErrors.upload_2}
                />
                <a
                  href={`${URL_HOST}${uploaded_2}`}
                  target="_blank"
                  className="uploaded_files"
                >
                  {uploaded_2}
                </a>
                <CInvalidFeedback>{addBankErrors.upload_2}</CInvalidFeedback>
              </CCol>

              <CCol md="3">
                <CLabel htmlFor="upload_3">
                  PAN Upload <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_3"
                  name="upload_3"
                  // value={upload_3}
                  onChange={(e) => handleInputValuesUpload2(e, "pan_upload")}
                  invalid={addBankErrors.upload_3}
                />
                <a
                  href={`${URL_HOST}${uploaded_3}`}
                  target="_blank"
                  className="uploaded_files"
                >
                  {uploaded_3}
                </a>
                <CInvalidFeedback>{addBankErrors.upload_3}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_4">
                  Agreement Upload <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_4"
                  name="upload_4"
                  // value={upload_4}
                  onChange={(e) =>
                    handleInputValuesUpload2(e, "agreement_upload")
                  }
                  invalid={addBankErrors.upload_4}
                />
                <a
                  href={`${URL_HOST}${uploaded_4}`}
                  target="_blank"
                  className="uploaded_files"
                >
                  {uploaded_4}
                </a>
                <CInvalidFeedback>{addBankErrors.upload_4}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_5">
                  Customer Logo <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_5"
                  name="upload_5"
                  // value={upload_5}
                  onChange={(e) =>
                    handleInputValuesUpload2(e, "customer_logo_upload")
                  }
                  invalid={addBankErrors.upload_5}
                />
                <a
                  href={`${URL_HOST}${uploaded_5}`}
                  target="_blank"
                  className="uploaded_files"
                >
                  {uploaded_5}
                </a>
                <CInvalidFeedback>{addBankErrors.upload_5}</CInvalidFeedback>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CCardFooter className="divider_layout"></CCardFooter>
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="upload_3">
                  Admin Username <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="admin_username"
                  name="admin_username"
                  placeholder="Username"
                  value={admin_username}
                  onChange={handleInputValues}
                  invalid={addBankErrors.admin_username}
                  disabled
                />
                <CInvalidFeedback>
                  {addBankErrors.admin_username}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_4">
                  Password <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  placeholder="Password"
                  onChange={handleInputValues}
                  autoComplete="new-password"
                  invalid={addBankErrors.password ? true : false}
                />
                <CInvalidFeedback>{addBankErrors.password}</CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_5">
                  Confirm Password <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="confirm_password"
                  name="confirm_password"
                  value={confirm_password}
                  onChange={handleInputValues}
                  placeholder="confirm_password"
                  autoComplete="password"
                  invalid={addBankErrors.confirm_password ? true : false}
                />

                <CInvalidFeedback>
                  {addBankErrors.confirm_password}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>
        <CCardFooter>
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleUpdateBank}
          >
            <CIcon name="cil-scrubber" /> Update {loading && <ContentLoading />}
          </CButton>
          <a href="#/bank-list" className="cancel_bt">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>

          {/* <CButton
            type="reset"
            size="sm"
            color="danger"
            className="reset_button"
          >
            <CIcon name="cil-ban" /> Reset
          </CButton> */}
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
