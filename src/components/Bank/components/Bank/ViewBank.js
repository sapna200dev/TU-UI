import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "./AddBankStyle.css";

export const ViewBank = (props) => {
  const {
    handleInputValues,
    handleAddBank,
    addBankErrors,
    resetMsg,
    errorMsg,
    bank_name,
    address,
    location,
    status,
    billing_status,
    company_email,
    contact_email,
    company_phone,
    contact_name,
    contact_phone,
    contact_location,
    billing_cycle,
    billing_cycle_from,
    billing_cycle_to,
    confirm_password,
    password,
    admin_username,
    upload_1,
    upload_2,
    upload_3,
    upload_4,
    upload_5,
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b> Bank Details</b>
          </h4>
        </CCardHeader>
        {resetMsg && (
          <CAlert color="success" className="msg_div">
            {resetMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="bank_name">
                  Bank Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="bank_name"
                  name="bank_name"
                  value={bank_name}
                  placeholder="Bank Name"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="branch_name">
                  HO Location <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="location"
                  name="location"
                  value={location}
                  placeholder="HO Location"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="address">
                  Address <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="address"
                  name="address"
                  value={address}
                  placeholder="Common Address"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="status">
                  Status <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="status"
                  id="status"
                  value={status}
                  disabled
                >
                  <option value="">Please select</option>
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="billing_status">
                  Billing Status <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="billing_status"
                  name="billing_status"
                  value={billing_status}
                  placeholder="Billing Status"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="company_email">
                  Compnay Email <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="company_email"
                  name="company_email"
                  value={company_email}
                  placeholder="Compnay Email"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="company_phone">
                  Company Phone <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="company_phone"
                  name="company_phone"
                  value={company_phone}
                  placeholder="Compnay Phone"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="contact_name">
                  Contact Person <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="contact_name"
                  name="contact_name"
                  value={contact_name}
                  placeholder="Contact Person"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="contact_phone">
                  Contact Person Phone{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="contact_phone"
                  name="contact_phone"
                  value={contact_phone}
                  placeholder="Contact Person Phone"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="contact_location">
                  Contact Person Email{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="contact_email"
                  name="contact_email"
                  value={contact_email}
                  placeholder="Contact Person Email"
                  disabled
                />
              </CCol>

              <CCol md="3">
                <CLabel htmlFor="contact_location">
                  Contact Person Location{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="contact_location"
                  name="contact_location"
                  value={contact_location}
                  placeholder="Contact Person Location"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="billing_cycle">
                  Billing Cycle <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="billing_cycle"
                  id="billing_cycle"
                  value={billing_cycle}
                  disabled
                >
                  <option value="">Please select</option>
                  <option value="quarterly">Quarterly</option>
                  <option value="monthly">Monthly</option>
                  <option value="yearly">Yearly</option>
                </CSelect>
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="billing_cycle_from">
                  Billing Cycle From <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="date"
                  id="billing_cycle_from"
                  name="billing_cycle_from"
                  value={billing_cycle_from}
                  placeholder="Billing Cycle From"
                  disabled
                />
              </CCol>

              <CCol md="3">
                <CLabel htmlFor="billing_cycle_to">
                  Billing Cycle To <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="date"
                  id="billing_cycle_to"
                  name="billing_cycle_to"
                  value={billing_cycle_to}
                  placeholder="Billing Cycle To"
                  disabled
                />
              </CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_1" name="upload_1">
                  COI Upload <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_1"
                  name="upload_1"
                  value={upload_1}
                  disabled
                />
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_2">
                  GST Upload <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_2"
                  name="upload_2"
                  value={upload_2}
                  disabled
                />
              </CCol>

              <CCol md="3">
                <CLabel htmlFor="upload_3">
                  PAN Upload <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_3"
                  name="upload_3"
                  value={upload_3}
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_4">
                  Agreement Upload <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_4"
                  name="upload_4"
                  value={upload_4}
                  disabled
                />
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>

              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_5">
                  Customer Logo <small className="required_lable">*</small>
                </CLabel>
                <CInputFile
                  id="upload_5"
                  name="upload_5"
                  value={upload_5}
                  disabled
                />
              </CCol>

              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CCardFooter className="divider_layout"></CCardFooter>
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="upload_3">
                  Admin Username <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="admin_username"
                  name="admin_username"
                  placeholder="Username"
                  value={admin_username}
                  disabled
                />
              </CCol>
              {/* <CCol xs="12" md="3">
                <CLabel htmlFor="upload_4">
                  Password <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  placeholder="Password"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="upload_5">
                  Confirm Password <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  type="password"
                  id="confirm_password"
                  name="confirm_password"
                  value={confirm_password}
                  placeholder="confirm_password"
                  disabled
                />
              </CCol> */}
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>
        <CCardFooter>
          <CButton type="submit" size="sm" color="primary">
            <CIcon name="cil-scrubber" />
            <a href="#/bank-list" className="cancel_bt">
              Go Back
            </a>
          </CButton>
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};
