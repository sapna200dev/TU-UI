import React, { Component } from "react";
import { connect } from 'react-redux';
import {
  CAlert,
  CButton,
  CCol,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend, 
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import "../../containers/common.css";
import {
  REGEX__EMAIL,
  TEXT__VALIDATION_ERROR__USERNAME,
  TEXT__VALIDATION_ERROR__PASSWORD_EMPTY,
} from "../../../Comman/constants";
import { settToken, redirectToDashboard, settUser, settUserObj, setLoggedInUser } from "../../../Comman/functions";
import { ContentLoading } from "../../../Comman/components";
import {setAuthHeader} from '../../../Comman/axiosConfig'
import { login } from "../../servicers/common";
import { attemptLogin } from '../../actions/index'



class LoginPage extends Component {
  state = {
    username: "",
    password: "",
    usernameError: "",
    passwordError: "",
    errorMsg: "",
    loading: false,
    all_roles:[],
    all_branches:[]
  };


  componentDidUpdate(prevProps) {
    const {loginPayload} = this.props
    if(prevProps.loginPayload != loginPayload) {
      console.log('login payload', loginPayload)
    }
  }


  validateInput = (e) => {
    const field = e.target.name;
    const val = e.target.value;
    switch (field) {
      case "username":
        this.validateUsername(val);
        break;
      case "password":
        this.validatePassword(val);
        break;

      default:
        console.warn("validateInput was supplied an uncaught field case");
        break;
    }
  };

  /**
   * Validate input username
   */
  validateUsername = (val) => {
    if (!REGEX__EMAIL.test(val)) {
      this.setState({ usernameError: TEXT__VALIDATION_ERROR__USERNAME });
      return true;
    }
    this.setState({ usernameError: "" });
    return false;
  };

  /**
   * Validate input passowrd
   */
  validatePassword = (val) => {
    if (val.length < 1) {
      this.setState({ passwordError: TEXT__VALIDATION_ERROR__PASSWORD_EMPTY });
      return true;
    }
    this.setState({ passwordError: "" });
    return false;
  };

  /**
   * Validate input username and password
   */
  validateRequired = () => {
    const usernameCheck = this.validateUsername(this.state.username);
    const passwordCheck = this.validatePassword(this.state.password);

    if (usernameCheck || passwordCheck) {
      return true;
    }
    return false;
  };

  /**
   * Handle inputs
   */
  handleChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  /**
   * Submit login form
   */
  submitForm = (e) => {
    e.preventDefault();

    // proceed if no validation errors
    const { username, password } = this.state;
    const result = this.validateRequired();

    if (!result) {
      this.setState({ loading: true });
      const data = {
        email: username,
        password: password,
      };

      this.props.login(username, password);
      login(data)
        .then((res) => {
          console.log("Success response = ", res.data);

          if (res.data && res.data.token) {
            // this.setState({ errorMsg: "", loading: false }); 
            // const userObj = JSON.stringify(res.data.roles);
            // const loggedInUser = {
            //   username:res.data.username,
            //   email:res.data.email,
            //   id: res.data.id,
            //   profile:res.data.profile
            // }
            // // Set user token in localstorage
            // console.log('user obj = ')
            // console.log(JSON.stringify(res.data))
            // settToken(res.data.token);
            // setAuthHeader(res.data.token)
            // settUser(res.data.username);

            // settUserObj(userObj);

            // setLoggedInUser(JSON.stringify(loggedInUser))
            // // after login success redirect to login
            // redirectToDashboard(true);
          } else {
            this.setState({ errorMsg: res.data.msg, loading: false });
          }
        })
        .catch((err) => {
          console.log("error reponse = ", err);
          this.setState({
            errorMsg: err.response ? err.response.data.msg : "",
            loading: false,
          });
        });
    }
  };




  render() {
    const {
      username,
      password,
      usernameError,
      passwordError,
      errorMsg,
      loading,
      all_branches,
      all_roles
    } = this.state;
    const {handleForgotPass} = this.props
    console.log("all_branches ", all_branches);
    console.log("all_roles ", all_roles);
    return (
      <div>
        <CForm>
          <h1>Login</h1>

          <p className="text-muted">Sign In to your account</p>
          {errorMsg && (
            <CAlert color="danger">
              {/*eslint-disable-next-line*/}
              {`Error : ${errorMsg}`}
            </CAlert>
          )}

          <CInputGroup className="mb-3 input_class">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CInput
              type="text"
              placeholder="Username"
              name="username"
              autoComplete="username"
              onChange={this.handleChangeInput}
              value={username}
              onBlur={(e) => {
                if (username.length > 0) {
                  this.validateInput(e);
                }
              }}
            />
          </CInputGroup>
          <div
            className="validationErrorMessagesStyles"
            style={{ textAlign: "left" }}
          >
            {usernameError}
          </div>
          <CInputGroup className="mb-4 input_class">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-lock-locked" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CInput
              type="password"
              placeholder="Password"
              name="password"
              autoComplete="password"
              onChange={this.handleChangeInput}
              value={password}
            />
          </CInputGroup>
          <div
            className="validationErrorMessagesStyles"
            style={{ textAlign: "left" }}
          >
            {passwordError}
          </div>
          <CRow>
            <CCol xs="6">
              <CButton
                color="primary"
                className="px-4 login_bt"
                onClick={this.submitForm}
              >
                Login
                {loading && <ContentLoading />}
              </CButton>
            </CCol>
            <CCol xs="6" className="text-right">
              <CButton color="link" className="px-0" onClick={handleForgotPass}>
                Forgot password?
              </CButton>
            </CCol>
          </CRow>
        </CForm>
      </div>
    );
  }
}



export function mapStateToProps(state) {
  return {
    isAttemptingLogin: state.loginReducer.isAttemptingLogin,
    loginPayload: state.loginReducer.payload,
    loginError: state.loginReducer.loginError,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    login: (email, password) => dispatch(attemptLogin(email, password)),
  };
}



export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

