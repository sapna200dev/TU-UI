import React, { useState } from "react";
import "../../containers/common.css";
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CRow,
} from "@coreui/react";
import { Forgot } from "../../components/Auth";
import LoginPage from "../../components/Auth/LoginPage";

export const Login = () => {
  const [isForgotPass, setIsForgotPass] = useState(false);

  /**
   * Display forgot page
   */
  const handleForgotPass = () => {
    setIsForgotPass(true);
  };

  /**
   * Display login page
   */
  const handleLoginPage = () => {
    setIsForgotPass(false);
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  {isForgotPass ? (
                    <Forgot handleLoginPage={handleLoginPage} />
                  ) : (
                    <LoginPage handleForgotPass={handleForgotPass} />
                  )}
                </CCardBody>
              </CCard>
              <CCard
                className="text-white bg-primary py-5 d-md-down-none"
                style={{ width: "44%" }}
              >
                <CCardBody className="text-center">
                  <div>
                    <h2>TURANT</h2>
                    {isForgotPass ? (
                      <p>
                        Please <b>Forgot</b> your password to <b>Login</b>.{" "}
                      </p>
                    ) : (
                      <p>
                        Please <b>Login</b> for get <b>Turant</b> access.{" "}
                      </p>
                    )}

                    {/* <Link to="/register">
                        <CButton color="primary" className="mt-3" active tabIndex={-1}>Register Now!</CButton>
                      </Link> */}
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
