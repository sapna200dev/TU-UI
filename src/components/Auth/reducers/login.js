// import { configureScope } from '@sentry/browser';
import * as ActionTypes from "../actions";
import { setAuthHeader } from "../../Comman/axiosConfig";
import {
  settToken,
  redirectToDashboard,
  settUser,
  settUserObj,
  setLoggedInUser,
} from "../../Comman/functions";

export const defaultLoginState = {
  isAttemptingLogin: false,
  payload: null,
  loginError: null,
  logginOut: false,
};

export function loginReducer(state = defaultLoginState, action) {
  switch (action.type) {
    case ActionTypes.ATTEMPT_LOGIN:
      return {
        ...state,
        isAttemptingLogin: true,
        beginLoginTime: new Date().getTime(),
      };

    case ActionTypes.LOGIN_SUCCESS: {
      const redirectingUser = false;

      const userObj = JSON.stringify(action.response.roles[0]);
      const loggedInUser = {
        username: action.response.username,
        email: action.response.email,
        id: action.response.id,
        profile: action.response.profile,
      };
      // Set user token in localstorage
      console.log("user obj = ");
      console.log(JSON.stringify(action.response));
      settToken(action.response.token);
      setAuthHeader(action.response.token);
      settUser(action.response.username);

      settUserObj(userObj);

      setLoggedInUser(JSON.stringify(loggedInUser));
      // after login success redirect to login
      redirectToDashboard(true);

      return {
        ...state,
        isAttemptingLogin: false,
        payload: action.response,
      };
    }
    case ActionTypes.LOGIN_FAILURE:
      return {
        ...state,
        isAttemptingLogin: false,
        payload: null,
      };

    case ActionTypes.LOGOUT_ACTION:
      document.cookie =
        "accessToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=ytel.com; path=/;";
      localStorage.userToken = "";

      return {
        ...defaultLoginState,
        logginOut: true,
      };
    case ActionTypes.LOGOUT_SUCCESS:
      document.cookie =
        "accessToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=ytel.com; path=/;";
      localStorage.removeItem("access_token");
      localStorage.removeItem("role");
      localStorage.removeItem("loggedInUser");
      localStorage.removeItem("username");

      return {
        ...defaultLoginState,
        logginOut: true,
      };

    default:
      return state;
  }
}
