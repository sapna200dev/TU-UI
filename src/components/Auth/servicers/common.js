import axios from "axios";
import {URL_HOST} from '../../Comman/constants'
// const domain = "http://sapna-dev.turant.com:8000";

export const login = (data) => axios.post(`${URL_HOST}/api/auth/login`, data);

export const logout = () => ({
  url: `${URL_HOST}/api/auth/logout`,
  headers: { Authorization: "Bearer " + localStorage.getItem("access_token") },
});

export const forgotPassword = (data) =>
  axios.post(`${URL_HOST}/api/auth/forgot-password`, data);

export const resetPassword = (data) =>
  axios.post(`${URL_HOST}/api/auth/reset-password`, data);


