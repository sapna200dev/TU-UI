import React, { Fragment } from "react";
import {
  CButton,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CInputFile,
  CLabel,
  CAlert,
  CInvalidFeedback,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ContentLoading } from "../../../Comman/components";

export const ProfileModal = (props) => {
  const {
    username,
    email,
    password,
    confirm_password,
    image,
    handleInputs,
    confirmPasswordError,
    usernameError,
    passwordError,
    emailError,
    handleUpdateProfile,
    loading,
    previewImage,
    blob,
    handleChange,
    resetMsg,
    errorMsg,
  } = props;

  return (
    <Fragment>
      <CCardHeader>Update Profile</CCardHeader>
      {resetMsg && (
        <CAlert color="success" className="msg_div">
          {resetMsg}
        </CAlert>
      )}
      {errorMsg && (
        <CAlert color="danger" className="msg_div">
          {errorMsg}
        </CAlert>
      )}
      <CCardBody>
        <CForm
          action=""
          method="post"
          encType="multipart/form-data"
          className="form-horizontal"
        >
          {/* <CFormGroup row>
                  <CCol md="3">
                    <CLabel>Static</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <p className="form-control-static">Username</p>
                  </CCol>
                </CFormGroup> */}
          <CFormGroup row>
            <CCol md="3">
              <CLabel htmlFor="inputIsInvalid">Username</CLabel>
            </CCol>
            <CCol xs="12" md="9">
              <CInput
                id="username"
                name="username"
                value={username}
                placeholder="Username..."
                onChange={handleInputs}
                invalid={usernameError ? true : false}
              />
              <CInvalidFeedback>{usernameError}</CInvalidFeedback>
            </CCol>
          </CFormGroup>
          <CFormGroup row>
            <CCol md="3">
              <CLabel htmlFor="email">Email </CLabel>
            </CCol>
            <CCol xs="12" md="9">
              <CInput
                type="email"
                id="email"
                name="email"
                value={email}
                placeholder="Enter Email"
                onChange={handleInputs}
                autoComplete="email"
                invalid={emailError ? true : false}
              />
              <CInvalidFeedback>{emailError}</CInvalidFeedback>
            </CCol>
          </CFormGroup>
          <CFormGroup row>
            <CCol md="3">
              <CLabel htmlFor="password">Password</CLabel>
            </CCol>
            <CCol xs="12" md="9">
              <CInput
                type="password"
                id="password"
                name="password"
                value={password}
                placeholder="Password"
                onChange={handleInputs}
                autoComplete="new-password"
                invalid={passwordError ? true : false}
              />
              <CInvalidFeedback>{passwordError}</CInvalidFeedback>
            </CCol>
          </CFormGroup>
          <CFormGroup row>
            <CCol md="3">
              <CLabel htmlFor="confirm_password">Confirm Password</CLabel>
            </CCol>
            <CCol xs="12" md="9">
              <CInput
                type="password"
                id="confirm_password"
                name="confirm_password"
                value={confirm_password}
                onChange={handleInputs}
                placeholder="confirm_password"
                autoComplete="password"
                invalid={confirmPasswordError ? true : false}
              />
              <CInvalidFeedback>{confirmPasswordError}</CInvalidFeedback>
            </CCol>
          </CFormGroup>
          <CFormGroup row>
            <CLabel col md="3" htmlFor="file-input" name="previewImage">
              Upload Image
            </CLabel>
            <CCol xs="12" md="9">
              <CInputFile
                id="file-input"
                name="file-input"
                onChange={handleChange}
              />
            </CCol>
          </CFormGroup>
          <CFormGroup row>
            <CLabel col md="3" htmlFor="file-input" name="previewImage">
              Image Preview
            </CLabel>
            <CCol xs="12" md="9">
              <img
                src={previewImage}
                className="img-fluid"
                style={{ width: "15%" }}
              />
            </CCol>
          </CFormGroup>
        </CForm>
      </CCardBody>
      <CCardFooter>
        <CButton
          type="submit"
          size="sm"
          color="primary"
          onClick={handleUpdateProfile}
        >
          <CIcon name="cil-scrubber" /> Update {loading && <ContentLoading />}
        </CButton>
        {/* <CButton type="reset" size="sm" color="danger"><CIcon name="cil-ban" /> Reset</CButton> */}
      </CCardFooter>
    </Fragment>
  );
};
