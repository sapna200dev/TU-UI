import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'


export const updateProfile = (id, data) =>
axios.post(`${URL_HOST}/api/profile/update/${id}`, data);
