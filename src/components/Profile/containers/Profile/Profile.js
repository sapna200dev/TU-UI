import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { CCard, CAlert } from "@coreui/react";
import { ProfileModal } from "../../components/Profile";
import { ERROR_VALIDATIONS } from "../../../Comman/constants";
import {
  getLoggedInUser,
  getUserObj,
  setLoggedInUser,
} from "../../../Comman/functions";
import { updateProfileFormRequest } from "../../functions";
// import { updateProfile } from "../../services";
import { updateProfile } from "../../actions/index";
import "./ProfileStyle.css";

class Profile extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    confirm_password: "",
    image: "",
    usernameError: "",
    passwordError: "",
    confirmPasswordError: "",
    emailError: "",
    loading: false,
    loggedInUser: "",
    previewImage: "",
    blob: "",
    id: "",
    resetMsg: "",
    errorMsg: "",
    role_name: "",
  };

  componentDidMount() {
    const loggedInUser = getLoggedInUser("loggedInUser");
    if (loggedInUser) {
      const user_role = getUserObj("role");
      const roleObj = JSON.parse(user_role);

      const userObj = JSON.parse(loggedInUser);

      console.log("user obj = ", userObj);
      this.setState({
        username: userObj.username,
        email: userObj.email,
        id: userObj.id,
        role_name: roleObj.display_name,
      });
    }
  }

  handleInputs = (e) => {
    const { password, confirm_password } = this.state;
    const { name, value } = e.target;

    if (name == "password" || name == "confirm_password") {
      const passwordCheck = this.validatePassword(password);
      const confirmPasswordCheck = this.validateConfirmPassword(
        confirm_password
      );

      if (passwordCheck || confirmPasswordCheck) {
        this.setState({ [name]: value });
        return true;
      }

      this.setState({ [name]: value });
      return false;
    }
    this.setState({ [name]: value });
  };

  /**
   * Validate input username
   */
  validateEmail = (val) => {
    if (!ERROR_VALIDATIONS.REGEX__EMAIL.test(val)) {
      this.setState({
        emailError: ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR__USERNAME,
      });
      return true;
    }
    this.setState({ emailError: "" });
    return false;
  };

  /**
   * Validate input username
   */
  validateUsername = (val) => {
    if (val.length < 1) {
      this.setState({
        usernameError: ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR__USERNAME_EMPTY,
      });
      return true;
    }
    this.setState({ usernameError: "" });
    return false;
  };

  /**
   * Validate input confirm_password with password
   */
  validateConfirmPassword = (val) => {
    const { password, confirm_password } = this.state;

    if (val.length < 1) {
      this.setState({
        confirmPasswordError:
          ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_EMPTY,
      });
      return true;
    } else if (password != confirm_password) {
      this.setState({
        confirmPasswordError:
          ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_UNMATCH,
      });
      return true;
    }

    this.setState({ confirmPasswordError: "" });
    return false;
  };

  /**
   * Validate input passowrd
   */
  validatePassword = (val) => {
    if (val.length < 1) {
      this.setState({
        passwordError: ERROR_VALIDATIONS.TEXT__VALIDATION_ERROR__PASSWORD_EMPTY,
      });
      return true;
    }
    this.setState({ passwordError: "" });
    return false;
  };

  /**
   * Validate input username and email
   */
  validateRequired = () => {
    const { username, email } = this.state;

    const usernameCheck = this.validateUsername(username);
    const emailCheck = this.validateEmail(email);

    if (usernameCheck || emailCheck) {
      return true;
    }
    return false;
  };

  /**
   * Get input file
   */
  handleChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        image: file,
        previewImage: reader.result,
      });
    };
    reader.readAsDataURL(file);
  };

  /**
   * Handle user profile
   */
  handleUpdateProfile = (e) => {
    e.preventDefault();
    const { id } = this.state;

    const result = this.validateRequired();
    if (!result) {
      let payload = updateProfileFormRequest(this.state);

      this.props.updateProfile(id, payload);
    }
  };

  render() {
    const {
      username,
      email,
      password,
      confirm_password,
      image,
      confirmPasswordError,
      usernameError,
      passwordError,
      emailError,

      previewImage,
      blob,
      role_name,
      resetMsg,
      errorMsg,
    } = this.state;

    console.log("reponse update = ", isSuccessMsg);
    const { isSuccessMsg, isLoading } = this.props;

    return (
      <Fragment>
        <CCard>
          <ProfileModal
            username={username}
            email={email}
            password={password}
            confirm_password={confirm_password}
            image={image}
            confirmPasswordError={confirmPasswordError}
            usernameError={usernameError}
            passwordError={passwordError}
            emailError={emailError}
            handleInputs={this.handleInputs}
            handleUpdateProfile={this.handleUpdateProfile}
            loading={isLoading}
            previewImage={previewImage}
            blob={blob}
            role_name={role_name}
            handleChange={this.handleChange}
            errorMsg={errorMsg}
            resetMsg={isSuccessMsg}
          />
        </CCard>
      </Fragment>
    );
  }
}

export function mapStateToProps(state) {
  return {
    isLoading: state.ProfileReducer.isLoading,
    payload: state.ProfileReducer.payload,
    isSuccessMsg: state.ProfileReducer.isSuccessMsg,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    updateProfile: (id, data) => dispatch(updateProfile(id, data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
