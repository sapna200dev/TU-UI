import React, { Fragment, useEffect, useState } from "react";
import {
  CButton,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CInputFile,
  CLabel,
  CAlert,
  CInvalidFeedback,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ContentLoading } from "../../../Comman/components";
import { URL_HOST } from "../../../Comman/constants";
import {getLoggedInUser} from '../../../Comman/functions'


const img_profile = {
  width: "31%",
  height: "119px",
  borderRadius: " 8px",
};

export const ProfileModal = (props) => {
  const {
    username,
    email,
    password,
    confirm_password,
    image,
    handleInputs,
    confirmPasswordError,
    usernameError,
    passwordError,
    emailError,
    handleUpdateProfile,
    loading,
    previewImage,
    blob,
    handleChange,
    resetMsg,
    errorMsg,

    role_name,
    role,
  } = props;


  const [profile, setProfile] = useState(null)

  useEffect(() => {

    const user_role = getLoggedInUser();
    const roleObj = JSON.parse(user_role);
    console.log('logged in user = ',roleObj )

    setProfile(roleObj.profile)
  }, [])

  return (
    <Fragment>
      <CCardHeader>
        <h4>
          <b> Update Profile</b>
        </h4>
      </CCardHeader>
      
      {resetMsg && (
                  <CAlert color="success" className="msg_div">
                    {resetMsg}
                  </CAlert>
                  )
                }
     


      
      {errorMsg && (
        <CAlert color="danger" className="msg_div">
          {errorMsg}
        </CAlert>
      )}
      <CCardBody>
        <CForm
          action=""
          method="post"
          encType="multipart/form-data"
          className="form-horizontal"
        >
          <CFormGroup row>
            <CCol xs="12" md="1">
              {" "}
            </CCol>
            <CCol md="3">
              <CLabel htmlFor="inputIsInvalid">Username</CLabel>
              <CInput
                id="username"
                name="username"
                value={username}
                placeholder="Username..."
                onChange={handleInputs}
                disabled
              />
              <CInvalidFeedback>{usernameError}</CInvalidFeedback>
            </CCol>
            <CCol md="3">
              <CLabel htmlFor="role_name">Role Name </CLabel>
              <CInput
                type="role_name"
                id="role_name"
                name="role_name"
                value={role_name}
                placeholder="Role Name"
                onChange={handleInputs}
                autoComplete="role_name"
                disabled
              />
              {/* <CInvalidFeedback>{emailError}</CInvalidFeedback> */}
            </CCol>

            <CCol md="3">
              <CLabel htmlFor="email">Role Name </CLabel>
              <CInput
                type="role_name"
                id="role_name"
                name="role_name"
                value={role_name}
                placeholder="Enter Role"
                onChange={handleInputs}
                autoComplete="role_name"
                disabled
              />
              {/* <CInvalidFeedback>{emailError}</CInvalidFeedback> */}
            </CCol>

            <CCol xs="12" md="1">
              {" "}
            </CCol>
          </CFormGroup>

          <CFormGroup row>
            <CCol xs="12" md="1">
              {" "}
            </CCol>
            <CCol md="3">
              <CLabel htmlFor="email">Email </CLabel>
              <CInput
                type="email"
                id="email"
                name="email"
                value={email}
                placeholder="Enter Email"
                onChange={handleInputs}
                autoComplete="email"
                invalid={emailError ? true : false}
              />
              <CInvalidFeedback>{emailError}</CInvalidFeedback>
            </CCol>
            <CCol md="3">
              <CLabel htmlFor="email">Password </CLabel>

              <CInput
                type="password"
                id="password"
                name="password"
                value={password}
                placeholder="Password"
                onChange={handleInputs}
                autoComplete="new-password"
                invalid={passwordError ? true : false}
              />
              <CInvalidFeedback>{passwordError}</CInvalidFeedback>
            </CCol>

            <CCol md="3">
              <CLabel htmlFor="email">Confirm Password </CLabel>
              <CInput
                type="password"
                id="confirm_password"
                name="confirm_password"
                value={confirm_password}
                onChange={handleInputs}
                placeholder="confirm_password"
                autoComplete="password"
                invalid={confirmPasswordError ? true : false}
              />
              <CInvalidFeedback>{confirmPasswordError}</CInvalidFeedback>
            </CCol>

            <CCol xs="12" md="1">
              {" "}
            </CCol>
          </CFormGroup>

          <CFormGroup row>
            <CCol xs="12" md="1">
              {" "}
            </CCol>
            <CCol md="3">
              <CLabel htmlFor="file-input" name="previewImage">
                Upload Image
              </CLabel>
              <CCol xs="12" md="9">
                <CInputFile
                  id="file-input"
                  name="file-input"
                  onChange={handleChange}
                />
              </CCol>
            </CCol>

            <CCol xs="12" md="1">
              {" "}
            </CCol>
          </CFormGroup>

          <CFormGroup row>
            <CCol xs="12" md="1">
              {" "}
            </CCol>

            <CLabel col md="3" htmlFor="file-input" name="previewImage">
              <p> Image Preview</p>
              <span>
               
                {(profile) && (
                  <img src={`${URL_HOST}${profile}`}  style={img_profile}/>
                )}
              </span>

              <span style={{marginLeft:'16px'}}>
                {previewImage && (
                  <img
                    src={previewImage}
                    className="img-fluid"
                    style={img_profile}
                  />
                )}
              
              </span>
            </CLabel>

            <CCol xs="12" md="1">
              {" "}
            </CCol>
          </CFormGroup>
        </CForm>
      </CCardBody>
      <CCardFooter>
        <CButton
          type="submit"
          size="sm"
          color="primary"
          onClick={handleUpdateProfile}
        >
          <CIcon name="cil-scrubber" /> Update {loading && <ContentLoading />}
        </CButton>
        <a href="#/dashboard" className="cancel_update">
          <CButton
            type="submit"
            size="sm"
            color="danger"
            style={{ marginLeft: "10px" }}
          >
            <CIcon name="cil-scrubber" /> Cancel 
          </CButton>
        </a>

        {/* <CButton type="reset" size="sm" color="danger"><CIcon name="cil-ban" /> Reset</CButton> */}
      </CCardFooter>
    </Fragment>
  );
};
