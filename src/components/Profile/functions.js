

export const updateProfileFormRequest = (data) => {
    // proceed if no validation errors
    const { username, password, email, image, id } = data;

    var formDataRequest = new FormData();
    if (image) {
        formDataRequest.append("image", image);
      }
      formDataRequest.append("username", username);
      formDataRequest.append("email", email);

      if(password) {
        formDataRequest.append("password", password);
      }
      


      return formDataRequest

}