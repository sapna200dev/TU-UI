export const END_USER_STATE = {
    first_name: "",
    last_name: "",
    registered_mobile: "",
    loan_account_number: "",
    loan_amount: "",
    alternate_phone: "",
    preferred_langauge: "",
    loan_type: "",
    branch_id: "",
    sale_agent_id: "",
    team_leader_id: "",
    registeration_trigger: "",
    verification_trigger: "",
    addEndUserErrors: {},
    errorMsg: "",
    successMsg: "",
    id:''
}

export const END_USER_FORM_VALIDATIONS = {
    FIRST_NAME:'First name is rquired',
    VALID_CHARS:'Must be valid chars',
    LAST_NAME:'Last name is required',
    VALID_MOBILE:'Please enter valid mobile number',
    REGISTERED_MOBILE:'Registered mobile number is required',
    LOAN_ACCOUNT_NUMBER:'Loan account number is required',
    LOAN_AMOUNT:'Loan amount ie required',
    LOAN_AMOUNT_VALID:'Enter valid loan amount',
    ASSIGN_SALE_AGENT:'Sale agent is required',
    ASSIGN_TEMA_LEADER:'Team leader is required',
    REGISTERED_TRIGGER:'Registeration trigger is required',
    VERIFICATION_TRIGGER:'Verification trigger is required',
    VALID_CHARS_LENGTH:'Name must be 2 to 10 chars'
}



export const FIELDS =  [
    "name",
    "loan_amount",
    "registered_mobile",
    "alternate_phone",
    "language_type",
    "Action",
  ]