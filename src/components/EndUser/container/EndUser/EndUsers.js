import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from "@coreui/react";
import EndUserLists from "../../components/EndUser/EndUserLists";
import { fetchEndUser } from "../../actions/EndUser/index";
import { deleteEndUser } from "../../services/common";

class EndUsers extends Component {
  state = {
    endusers: [],
    deleteItem: "",
    isDelete: false,
    searchValue: "",
    filterEndUsers: [],
  };

  componentDidMount() {
    const { fetchEndUser } = this.props;

    fetchEndUser();
  }

  componentDidUpdate(prevProps) {
    const { endUsers } = this.props;
    if (prevProps.endUsers != endUsers) {
      this.setState({
        endUsers,
        filterEndUsers: endUsers,
      });
    }
  }

  /**
   * Handle delete event
   */
  handleDeleteItem = (item) => {
    this.setState({ isDelete: true, deleteItem: item });
  };

  /**
   * Handle cancel event
   */
  handleCancelModal = () => {
    this.setState({ isDelete: false, deleteItem: "" });
  };

  /**
   * Handle delete event
   */
  handleDeleteConfirm = () => {
    const ids = [];
    const { deleteItem } = this.state;

    deleteEndUser(deleteItem.id)
      .then((res) => {
        if (res.data && res.data.status == 200) {
          this.setState({
            successMsg: res.data.msg,
            loading: false,
            errorMsg: "",
            isDelete: false,
          });

          this.props.fetchEndUser();
          let _this = this;
          setTimeout(function () {
            _this.setState({ successMsg: "" });
          }, 3000);
        } else {
          this.setState({
            errorMsg: res.data.msg,
            loading: false,
            successMsg: "",
          });
        }

        this.getAllEmployess();
      })
      .catch((err) => {
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
      });
  };

  /**
   * Handle search event
   */
  handleFilterChange = (e) => {
    const data = e.target.value;
    const { endUsers } = this.props;

    let arrayEmployeeLists = [];
    if (data) {
      arrayEmployeeLists = this.handleFilterChangeVal(endUsers, data);
    } else {
      arrayEmployeeLists = this.props.endUsers;
    }

    this.setState({
      filterEndUsers: arrayEmployeeLists,
      searchValue: data,
    });
  };

  handleFilterChangeVal = (endusers, value) => {
    let employeeList = [];
    employeeList = endusers.filter((item) => {
      return (
        (item.name &&
          item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.loan_amount &&
          item.loan_amount.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
        (item.registered_mobile &&
          item.registered_mobile.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.alternate_phone &&
          item.alternate_phone.toLowerCase().indexOf(value.toLowerCase()) !==
            -1) ||
        (item.language_type &&
          item.language_type.toLowerCase().indexOf(value.toLowerCase()) !== -1)
      );
    });

    return employeeList;
  };

  render() {
    const { endUsers, isLoading } = this.props;
    const {
      isDelete,
      deleteItem,
      successMsg,
      errorMsg,
      searchValue,
      filterEndUsers,
    } = this.state;

    return (
      <Fragment>
        <EndUserLists
          endUsers={filterEndUsers}
          isLoading={isLoading}
          handleDeleteItem={this.handleDeleteItem}
          successMsg={successMsg}
          errorMsg={errorMsg}
          searchValue={searchValue}
          handleFilterChange={this.handleFilterChange}
        />

        {isDelete && (
          <CModal
            show={isDelete}
            onClose={this.handleCancelModal}
            color="danger"
          >
            <CModalHeader closeButton>
              <CModalTitle style={{ color: "white" }}>Delete</CModalTitle>
            </CModalHeader>
            <CModalBody>
              Are you sure to delete this End User <b>{deleteItem.name}</b>
            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={this.handleDeleteConfirm}>
                Delete
              </CButton>{" "}
              <CButton color="secondary" onClick={this.handleCancelModal}>
                Cancel
              </CButton>
            </CModalFooter>
          </CModal>
        )}
      </Fragment>
    );
  }
}

export function mapStateToProps(state) {
  return {
    endUsers: state.fetchEndUserReducer.endUsers,
    isLoading: state.fetchEndUserReducer.isLoading,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchEndUser: () => dispatch(fetchEndUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EndUsers);

// export default EndUsers
