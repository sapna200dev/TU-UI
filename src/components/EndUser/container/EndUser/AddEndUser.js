import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import AddEndUserModal from "../../components/EndUser/AddEndUserModal";
import {
  fetchLanguages,
  fetchLoanTypes,
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../actions/EndUser/index";
import { getBranches } from "../../../Comman/actions";
import { addNewEndUser } from "../../services/common";
import { END_USER_STATE } from "../../constant";
import { validateRequestParams, CreateReuqestPayload } from "../../functions";

class AddEndUser extends Component {
  state = END_USER_STATE;

  componentDidMount() {
    const {
      fetchLanguages,
      fetchLoanTypes,
      fetchSaleAgents,
      fetchTeamLeads,
      getBranches,
    } = this.props;
    fetchLanguages();
    fetchLoanTypes();
    fetchSaleAgents();
    fetchTeamLeads();
    getBranches();
  }

  /**
   * Handle Bank inputs
   */
  handleInputValues = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  /**
   * Handle bank validation
   */
  handleAddEndUserValidation = () => {
    // Request validate
    let addEndUserErrors = validateRequestParams(this.state);
    this.setState({ addEndUserErrors });
    return addEndUserErrors;
  };

  handleAddEndUser = () => {
    const errors = this.handleAddEndUserValidation();
    if (Object.getOwnPropertyNames(errors).length === 0) {
      this.addNewEndUser();
    }
  };

  addNewEndUser = () => {
    this.setState({ loading: true });
    let payload = CreateReuqestPayload(this.state);
    let _this = this;

    addNewEndUser(payload)
      .then((response) => {
        if (response.data && response.data.status == 200) {
          this.setState({
            successMsg: response.data.msg,
            loading: false,
            errorMsg: "",
          });

          setTimeout(function () {
            _this.setState({ successMsg: "" });
            _this.props.history.push("/end-user");
          }, 4000);
        } else {
          this.setState({
            errorMsg: response.data.msg,
            loading: false,
            successMsg: "",
          });

          setTimeout(function () {
            _this.setState({ errorMsg: "" });
          }, 4000);
        }
      })
      .catch((err) => {
        console.log("error => ", err.response);
        this.setState({
          errorMsg: err.response ? err.response.data.msg : "",
          loading: false,
        });
        setTimeout(function () {
          _this.setState({ errorMsg: "" });
        }, 4000);
      });
  };

  /**
   * Reset form
   */
  handleResetEvent = () => {
    this.setState({
      first_name: "",
      last_name: "",
      registered_mobile: "",
      loan_account_number: "",
      loan_amount: "",
      alternate_phone: "",
      preferred_langauge: "",
      loan_type: "",
      branch_id: "",
      sale_agent_id: "",
      team_leader_id: "",
      registeration_trigger: "",
      verification_trigger: "",
    });
  };

  render() {
    const {
      sale_agents,
      team_leads,
      languages,
      loan_types,
      branches,
    } = this.props;
    const {
      first_name,
      last_name,
      registered_mobile,
      loan_account_number,
      loan_amount,
      alternate_phone,
      preferred_langauge_id,
      loan_type_id,
      branch_id,
      sale_agent_id,
      team_leader_id,
      registeration_trigger,
      verification_trigger,
      addEndUserErrors,
      successMsg,
      errorMsg,
      loading,
    } = this.state;

    return (
      <Fragment>
        <AddEndUserModal
          loan_types={loan_types}
          languages={languages}
          team_leads={team_leads}
          sale_agents={sale_agents}
          branches={branches}
          handleInputValues={this.handleInputValues}
          first_name={first_name}
          last_name={last_name}
          errorMsg={errorMsg}
          successMsg={successMsg}
          registered_mobile={registered_mobile}
          loan_account_number={loan_account_number}
          loan_amount={loan_amount}
          alternate_phone={alternate_phone}
          preferred_langauge_id={preferred_langauge_id}
          loan_type_id={loan_type_id}
          branch_id={branch_id}
          sale_agent_id={sale_agent_id}
          team_leader_id={team_leader_id}
          registeration_trigger={registeration_trigger}
          verification_trigger={verification_trigger}
          handleAddEndUser={this.handleAddEndUser}
          addEndUserErrors={addEndUserErrors}
          loading={loading}
          handleResetEvent={this.handleResetEvent}
        />
      </Fragment>
    );
  }
}

// export default AddEndUser

export function mapStateToProps(state) {
  return {
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    languages: state.fetchEndUserReducer.languages,
    loan_types: state.fetchEndUserReducer.loan_types,
    branches: state.fetchBranchesReducer.branches,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchLanguages: () => dispatch(fetchLanguages()),
    fetchLoanTypes: () => dispatch(fetchLoanTypes()),
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),
    getBranches: () => dispatch(getBranches()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEndUser);
