import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import EndUserDetailView from "../../components/EndUser/EndUserDetailView";
import {
  fetchLanguages,
  fetchLoanTypes,
  fetchSaleAgents,
  fetchTeamLeads,
} from "../../actions/EndUser/index";
import { getBranches } from "../../../Comman/actions";
import {  getEndUserDetails } from "../../services/common";
import {END_USER_STATE} from '../../constant'

class EndUserDetails extends Component {
  state = END_USER_STATE;

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({ id });
    console.log('response  id= ', id)
    const {
      fetchLanguages,
      fetchLoanTypes,
      fetchSaleAgents,
      fetchTeamLeads,
      getBranches,
    } = this.props;
    fetchLanguages();
    fetchLoanTypes();
    fetchSaleAgents();
    fetchTeamLeads();
    getBranches();

    this.getEndUserById(id);
  }


getEndUserById = (id) => {
    getEndUserDetails(id).then(res => {

        let _this = this;
        this.setState({
            first_name: res.data.data.first_name,
            last_name: res.data.data.last_name,
            registered_mobile: res.data.data.registered_mobile,
            loan_account_number: res.data.data.loan_account_number,
            loan_amount:  res.data.data.loan_amount,
            alternate_phone: res.data.data.alternate_phone,
            preferred_langauge: res.data.data.preferred_langauge_id,
            loan_type: res.data.data.loan_type_id,
            branch_id: res.data.data.branch_id,
            sale_agent_id: res.data.data.sale_agent_id,
            team_leader_id: res.data.data.team_leader_id,
            registeration_trigger: res.data.data.registeration_trigger,
            verification_trigger: res.data.data.verification_trigger,
        })
    }).catch(err => {
        console.log('error => ', err.response)
        this.setState({
            errorMsg: err.response ? err.response.data.msg : "",
            loading: false,
          });
          setTimeout(function () {
            this.setState({ errorMsg: "" });
          }, 4000);
    })
}


  render() {
    const {
      sale_agents,
      team_leads,
      languages,
      loan_types,
      branches,
    } = this.props;
    const {
      first_name,
      last_name,
      registered_mobile,
      loan_account_number,
      loan_amount,
      alternate_phone,
      preferred_langauge,
      loan_type,
      branch_id,
      sale_agent_id,
      team_leader_id,
      registeration_trigger,
      verification_trigger,
      addEndUserErrors,
      successMsg,
      errorMsg,
      loading
    } = this.state;


    return (
      <Fragment>
        <EndUserDetailView
          loan_types={loan_types}
          languages={languages}
          team_leads={team_leads}
          sale_agents={sale_agents}
          branches={branches}
          handleInputValues={this.handleInputValues}
          first_name={first_name}
          last_name={last_name}
          errorMsg={errorMsg}
          successMsg={successMsg}
          registered_mobile={registered_mobile}
          loan_account_number={loan_account_number}
          loan_amount={loan_amount}
          alternate_phone={alternate_phone}
          preferred_langauge={preferred_langauge}
          loan_type={loan_type}
          branch_id={branch_id}
          sale_agent_id={sale_agent_id}
          team_leader_id={team_leader_id}
          registeration_trigger={registeration_trigger}
          verification_trigger={verification_trigger}
          handleEditEndUser={this.handleEditEndUser}
          addEndUserErrors={addEndUserErrors}
          loading={loading}
        />
      </Fragment>
    );
  }
}

// export default AddEndUser

export function mapStateToProps(state) {
  return {
    sale_agents: state.fetchEndUserReducer.sale_agents,
    team_leads: state.fetchEndUserReducer.team_leads,
    languages: state.fetchEndUserReducer.languages,
    loan_types: state.fetchEndUserReducer.loan_types,
    branches: state.fetchBranchesReducer.branches,
  };
}
export function mapDispatchToProps(dispatch) {
  return {
    fetchLanguages: () => dispatch(fetchLanguages()),
    fetchLoanTypes: () => dispatch(fetchLoanTypes()),
    fetchSaleAgents: () => dispatch(fetchSaleAgents()),
    fetchTeamLeads: () => dispatch(fetchTeamLeads()),
    getBranches: () => dispatch(getBranches()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EndUserDetails);
