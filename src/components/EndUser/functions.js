
import {END_USER_FORM_VALIDATIONS} from './constant'

/**
 * Handle request
 * @param  props
 */
export const validateRequestParams = (props) => {
    const addEndUserErrors = {};
    const {
        first_name,
        last_name,
        registered_mobile,
        loan_account_number,
        loan_amount,
        alternate_phone,
        preferred_langauge,
        loan_type,
        branch_id,
        sale_agent_id,
        team_leader_id,
        registeration_trigger,
        verification_trigger
    } = props;
  
    if (first_name.length < 1) {
      addEndUserErrors.first_name = END_USER_FORM_VALIDATIONS.FIRST_NAME;
    } else if (first_name.length < 2 || first_name.length > 10) {
        addEndUserErrors.first_name = END_USER_FORM_VALIDATIONS.VALID_CHARS_LENGTH;
    } else if (!/^[a-zA-Z]*$/g.test(first_name)) {
      addEndUserErrors.first_name = END_USER_FORM_VALIDATIONS.VALID_CHARS;
    } else if (last_name.length < 1) {
      addEndUserErrors.last_name = END_USER_FORM_VALIDATIONS.LAST_NAME;
    } else if (last_name.length < 2 || last_name.length > 10) {
        addEndUserErrors.last_name = END_USER_FORM_VALIDATIONS.VALID_CHARS_LENGTH;
    } else if (!/^[a-zA-Z]*$/g.test(last_name))  {
      addEndUserErrors.last_name = END_USER_FORM_VALIDATIONS.VALID_CHARS;
    } else if (registered_mobile.length < 1) {
      addEndUserErrors.registered_mobile = END_USER_FORM_VALIDATIONS.REGISTERED_MOBILE;
    } 
    // else if (/^[0-9]+$/.test(registered_mobile)) {
    //   addEndUserErrors.registered_mobile = END_USER_FORM_VALIDATIONS.VALID_MOBILE;
    // }
     else if (loan_account_number.length < 1) {
      addEndUserErrors.loan_account_number = END_USER_FORM_VALIDATIONS.LOAN_ACCOUNT_NUMBER;
    } else if (loan_amount.length < 1) {
      addEndUserErrors.loan_amount = END_USER_FORM_VALIDATIONS.LOAN_AMOUNT;
    } else if (!/^[0-9]+$/.test(loan_amount)) {
      addEndUserErrors.loan_amount = END_USER_FORM_VALIDATIONS.LOAN_AMOUNT_VALID;
    } else if (!/^[0-9]+$/.test(alternate_phone)) {
      addEndUserErrors.alternate_phone = END_USER_FORM_VALIDATIONS.VALID_MOBILE;
    } else if (sale_agent_id.length < 1) {
      addEndUserErrors.sale_agent_id = END_USER_FORM_VALIDATIONS.ASSIGN_SALE_AGENT;
    } 
    // else if (team_leader_id.length < 1) {
    //   addEndUserErrors.team_leader_id = END_USER_FORM_VALIDATIONS.ASSIGN_TEMA_LEADER;
    // }
  
    return addEndUserErrors;
  };




/**
 * Create request payload
 * @param props 
 */
  export const CreateReuqestPayload = (props) => {
    const {
        first_name,
        last_name,
        registered_mobile,
        loan_account_number,
        loan_amount,
        alternate_phone,
        preferred_langauge,
        loan_type,
        branch_id,
        sale_agent_id,
        team_leader_id,
        registeration_trigger,
        verification_trigger,
      } = props;

      console.log('payload  props= ',props)

      const payload = {
        first_name,
        last_name,
        registered_mobile,
        loan_account_number,
        loan_amount,
        alternate_phone,
        preferred_langauge_id:preferred_langauge,
        loan_type_id:loan_type,
        branch_id,
        sale_agent_id,
        team_leader_id,
        registeration_trigger,
        verification_trigger,
      }

      return payload;
  }