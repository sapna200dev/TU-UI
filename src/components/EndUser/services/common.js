import axios from 'axios'
import {URL_HOST} from '../../Comman/constants'


export const addNewEndUser = (data) =>
axios.post(`${URL_HOST}/api/end_user/add`, data);


export const getEndUserDetails = (id) =>
axios.get(`${URL_HOST}/api/end_user/${id}`);



export const updateEndUserDetails = (id, data) =>
    axios.post(`${URL_HOST}/api/end_user/edit/${id}`, data);

export const deleteEndUser = (id) =>
    axios.get(`${URL_HOST}/api/end_user/delete/${id}`);
    

