import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

const EndUserDetailView = (props) => {
  const {
    sale_agents,
    team_leads,
    branches,
    languages,
    loan_types,
    first_name,
    last_name,
    registered_mobile,
    loan_account_number,
    loan_amount,
    alternate_phone,
    preferred_langauge,
    loan_type,
    branch_id,
    sale_agent_id,
    team_leader_id,
    registeration_trigger,
    verification_trigger,
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b> End User Details</b>
          </h4>
        </CCardHeader>

        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="first_name">
                  First Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="first_name"
                  name="first_name"
                  value={first_name}
                  placeholder="First Name"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Last Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="last_name"
                  name="last_name"
                  value={last_name}
                  placeholder=" Last Name "
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="registered_mobile">
                  Registered Mobile<small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="registered_mobile"
                  name="registered_mobile"
                  value={registered_mobile}
                  placeholder="Registered Mobile"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_account_number">
                  Loan Account Number{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_account_number"
                  name="loan_account_number"
                  value={loan_account_number}
                  placeholder="Loan Account Number"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_amount">
                  Loan Amount <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_amount"
                  name="loan_amount"
                  value={loan_amount}
                  placeholder=" Loan Amount"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">Alternate Phone</CLabel>
                <CInput
                  id="alternate_phone"
                  name="alternate_phone"
                  value={alternate_phone}
                  placeholder="Alternate Phone"
                  disabled
                />
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="preferred_langauge">Preferred Language</CLabel>
                <CSelect
                  custom
                  name="preferred_langauge"
                  id="preferred_langauge"
                  disabled
                  value={preferred_langauge}
                >
                  <option value="">Please select</option>
                  {languages ? (
                    languages.map((item) => (
                      <option value={item.id}>{item.language}</option>
                    ))
                  ) : (
                    <option value="">No Languages</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_type">Loan Type</CLabel>
                <CSelect
                  custom
                  name="loan_type"
                  id="loan_type"
                  value={loan_type}
                  disabled
                >
                  <option value="">Please select</option>
                  {loan_types ? (
                    loan_types.map((item) => (
                      <option value={item.id}>{item.loan_type}</option>
                    ))
                  ) : (
                    <option value="">No Loan Types</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">Assign Branch</CLabel>
                <CSelect
                  custom
                  name="branch_id"
                  id="branch_id"
                  disabled
                  value={branch_id}
                >
                  <option value="">Please select</option>
                  {branches ? (
                    branches.map((item) => (
                      <option value={item.id}>{item.branch_name}</option>
                    ))
                  ) : (
                    <option value="">No Branch</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="sale_agent_id">
                  Assign Sales Agent<small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="sale_agent_id"
                  id="sale_agent_id"
                  disabled
                  value={sale_agent_id}
                >
                  <option value="">Please select</option>
                  {sale_agents ? (
                    sale_agents.map((item) => (
                      <option
                        value={item.id}
                        selected={item.id === sale_agent_id ? "selected" : ""}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Sales Agents</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="team_leader_id">
                  Assign Team Leader <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="team_leader_id"
                  id="team_leader_id"
                  disabled
                  value={team_leader_id}
                >
                  <option value="">Please select</option>
                  {team_leads ? (
                    team_leads.map((item) => (
                      <option
                        value={item.id}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Team Leads</option>
                  )}
                </CSelect>
              </CCol>
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="registeration_trigger">
                  Registeration Trigger
                  <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="registeration_trigger"
                  id="registeration_trigger"
                  disabled
                  value={registeration_trigger}
                >
                  <option value="">Please select</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                </CSelect>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="verification_trigger">
                  Verification Trigger{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="verification_trigger"
                  id="verification_trigger"
                  disabled
                  value={verification_trigger}
                >
                  <option value="">Please select</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                </CSelect>
              </CCol>
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>

        <CCardFooter>
          <a href="#/end-user" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Go Back
            </CButton>
          </a>
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};

export default EndUserDetailView;
