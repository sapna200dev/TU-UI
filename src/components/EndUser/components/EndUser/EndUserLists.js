import React, { Fragment } from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CAlert,
} from "@coreui/react";
import {FIELDS} from '../../constant'
// const fields = [
//   "name",
//   "loan_amount",
//   "registered_mobile",
//   "alternate_phone",
//   "language_type",
//   "Action",
// ];

const EndUserLists = (props) => {
  const {
    endUsers,
    isLoading,
    handleDeleteItem,
    successMsg,
    errorMsg,
    handleFilterChange,
    searchValue,
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <b style={{ fontSize: "20px" }}> End Users</b>
          <span>
            <div className="md-form mt-3">
              <input
                className="form-control search_list"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={(e) => {
                  handleFilterChange(e);
                }}
              />

              <a href="#/end-user-add" className="button_style">
                <CButton
                  type="submit"
                  size="sm"
                  color="primary"
                  className="add_employee_button"
                  // onClick={() => handleDeleteItem(item)}
                >
                  {" "}
                  Add End User
                </CButton>
              </a>
            </div>
          </span>
          {successMsg && (
            <CAlert color="success" className="msg_div">
              {successMsg}
            </CAlert>
          )}
          {errorMsg && (
            <CAlert color="danger" className="msg_div">
              {errorMsg}
            </CAlert>
          )}
          <CCardBody>
            <CDataTable
              items={endUsers}
              fields={FIELDS}
              itemsPerPage={5}
              loading={isLoading}
              pagination
              scopedSlots={{
                Action: (item) => (
                  <td>
                    <a
                      href={`#/end-user-edit/${item.id}`}
                      className="cancel_bt"
                    >
                      <CButton type="submit" size="sm" color="primary">
                        Edit
                        {/* {loading && <ContentLoading />} */}
                      </CButton>
                    </a>

                    <CButton
                      type="submit"
                      size="sm"
                      color="danger"
                      className="remove_button"
                      onClick={() => handleDeleteItem(item)}
                    >
                      Delete
                    </CButton>
                    <a
                      href={`#/end-user-view/${item.id}`}
                      className="cancel_bt"
                    >
                      <CButton
                        type="submit"
                        size="sm"
                        color="primary"
                        className="remove_button"
                      >
                        View
                      </CButton>
                    </a>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCardHeader>
      </CCard>
    </Fragment>
  );
};

export default EndUserLists;
