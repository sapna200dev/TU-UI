import React, { Fragment } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CInvalidFeedback,
  CInputFile,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { ContentLoading } from "../../../Comman/components";

const AddEndUserModal = (props) => {
  const {
    sale_agents,
    team_leads,
    languages,
    loan_types,
    handleInputValues,
    first_name,
    last_name,
    registered_mobile,
    loan_account_number,
    loan_amount,
    alternate_phone,
    preferred_langauge,
    loan_type,
    branch_id,
    sale_agent_id,
    team_leader,
    registeration_trigger,
    verification_trigger,
    handleAddEndUser,
    addEndUserErrors,
    branches,
    errorMsg,
    successMsg,
    loading,
    handleResetEvent
  } = props;

  return (
    <Fragment>
      <CCard>
        <CCardHeader>
          <h4>
            <b> Add End User</b>
          </h4>
        </CCardHeader>
        {successMsg && (
          <CAlert color="success" className="msg_div">
            {successMsg}
          </CAlert>
        )}
        {errorMsg && (
          <CAlert color="danger" className="msg_div">
            {errorMsg}
          </CAlert>
        )}
        <CCardBody>
          <CForm
            action=""
            method="post"
            encType="multipart/form-data"
            className="form-horizontal"
          >
            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="first_name">
                  First Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="first_name"
                  name="first_name"
                  value={first_name}
                  placeholder="First Name"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.first_name}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.first_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="last_name">
                  Last Name <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="last_name"
                  name="last_name"
                  value={last_name}
                  placeholder=" Last Name "
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.last_name}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.last_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="registered_mobile">
                  Registered Mobile<small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="registered_mobile"
                  name="registered_mobile"
                  value={registered_mobile}
                  placeholder="Registered Mobile"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.registered_mobile}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.registered_mobile}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="loan_account_number">
                  Loan Account Number{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_account_number"
                  name="loan_account_number"
                  value={loan_account_number}
                  placeholder="Loan Account Number"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_account_number}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.loan_account_number}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_amount">
                  Loan Amount <small className="required_lable">*</small>
                </CLabel>
                <CInput
                  id="loan_amount"
                  name="loan_amount"
                  value={loan_amount}
                  placeholder=" Loan Amount"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.loan_amount}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.loan_amount}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Alternate Phone
                </CLabel>
                <CInput
                  id="alternate_phone"
                  name="alternate_phone"
                  value={alternate_phone}
                  placeholder="Alternate Phone"
                  onChange={handleInputValues}
                  invalid={addEndUserErrors.alternate_phone}
                />
                <CInvalidFeedback>
                  {addEndUserErrors.alternate_phone}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="preferred_langauge">
                  Preferred Language
                </CLabel>
                <CSelect
                  custom
                  name="preferred_langauge"
                  id="preferred_langauge"
                  invalid={addEndUserErrors.preferred_langauge}
                  onChange={handleInputValues}
                  value={preferred_langauge}
                >
                  <option value="">Please select</option>
                  {languages ? (
                    languages.map((item) => (
                      <option value={item.id}>{item.language}</option>
                    ))
                  ) : (
                    <option value="">No Languages</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.first_name}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="loan_type">
                  Loan Type
                </CLabel>
                <CSelect
                  custom
                  name="loan_type"
                  id="loan_type"
                  invalid={addEndUserErrors.loan_type}
                  onChange={handleInputValues}
                  value={loan_type}
                >
                  <option value="">Please select</option>
                  {loan_types ? (
                    loan_types.map((item) => (
                      <option value={item.id}>{item.loan_type}</option>
                    ))
                  ) : (
                    <option value="">No Loan Types</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.loan_type}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="alternate_phone">
                  Assign Branch
                </CLabel>
                <CSelect
                  custom
                  name="branch_id"
                  id="branch_id"
                  invalid={addEndUserErrors.branch_id}
                  onChange={handleInputValues}
                  value={branch_id}
                >
                  <option value="">Please select</option>
                  {branches ? (
                    branches.map((item) => (
                      <option value={item.id}>{item.branch_name}</option>
                    ))
                  ) : (
                    <option value="">No Branch</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.branch_id}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="sale_agent_id">
                  Assign Sales Agent<small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="sale_agent_id"
                  id="sale_agent_id"
                  invalid={addEndUserErrors.sale_agent_id}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  {sale_agents ? (
                    sale_agents.map((item) => (
                      <option
                        value={item.id}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Sales Agents</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.sale_agent_id}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="team_leader">
                  Assign Team Leader <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="team_leader"
                  id="team_leader"
                  invalid={addEndUserErrors.team_leader}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  {team_leads ? (
                    team_leads.map((item) => (
                      <option
                        value={item.id}
                      >{`${item.first_name}  ${item.last_name}`}</option>
                    ))
                  ) : (
                    <option value="">No Team Leads</option>
                  )}
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.team_leader}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>

            <CFormGroup row>
              <CCol xs="12" md="1"></CCol>
              <CCol md="3">
                <CLabel htmlFor="registeration_trigger">
                  Registeration Trigger
                  <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="registeration_trigger"
                  id="registeration_trigger"
                  invalid={addEndUserErrors.registeration_trigger}
                  onChange={handleInputValues}
                >
                  <option value="">Please select</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.registeration_trigger}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3">
                <CLabel htmlFor="verification_trigger">
                  Verification Trigger{" "}
                  <small className="required_lable">*</small>
                </CLabel>
                <CSelect
                  custom
                  name="verification_trigger"
                  id="verification_trigger"
                  invalid={addEndUserErrors.verification_trigger}
                  onChange={handleInputValues}
                  disabled
                >
                  <option value="">Please select</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                </CSelect>
                <CInvalidFeedback>
                  {addEndUserErrors.verification_trigger}
                </CInvalidFeedback>
              </CCol>
              <CCol xs="12" md="3"></CCol>
              <CCol xs="12" md="2"></CCol>
            </CFormGroup>
          </CForm>
        </CCardBody>

        <CCardFooter>
          <CButton
            type="submit"
            size="sm"
            color="primary"
            onClick={handleAddEndUser}
          >
            <CIcon name="cil-scrubber" /> Save
            {loading && <ContentLoading />}
          </CButton>
          <a href="#/end-user" className="button_style">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              className="remove_button"
            >
              <CIcon name="cil-ban" /> Cancel
            </CButton>
          </a>

          <CButton
            type="reset"
            size="sm"
            color="danger"
            className="reset_button"
            onClick={handleResetEvent}
          >
            <CIcon name="cil-ban" /> Reset
          </CButton>
        </CCardFooter>
      </CCard>
    </Fragment>
  );
};

export default AddEndUserModal;
