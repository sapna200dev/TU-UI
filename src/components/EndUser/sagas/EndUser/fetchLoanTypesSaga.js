import { take, put, call, all } from 'redux-saga/effects';
import * as actions from '../../actions/EndUser';
import axios from '../../../Comman/axiosConfig';
import { URL_HOST } from '../../../Comman/constants'



export const fetchLoanTypesSaga = async () => {
    try {
      const response = await axios.get(`${URL_HOST}/api/loan/types`);

      return response.data;
    } catch (err) {
      return err.response.data;
    }
  };
  

  export function* fetchLoanTypes() {
    try {
      const response = yield fetchLoanTypesSaga();
      if (response && response.status == 200) {

        yield all([
          put({
            type: actions.FETCH_LOAN_TYPES_SUCCESS, 
            response,
          })
       
        ]);
      } else {
  
        yield all([
          put({
            type: actions.FETCH_LOAN_TYPES_FAILURE,
            response,

          })
        ]);
      }
    } catch (err) {
      yield all([
        put({ type: actions.FETCH_LOAN_TYPES_FAILURE, err })
       
      ]);
    }
  }



  export function* watchFetchLoanTypeSaga() {
    while (true) {
      const state = yield take(actions.FETCH_LOAN_TYPES);
      yield call(fetchLoanTypes);
    }
  }
  