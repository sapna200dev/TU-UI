

export const REGEX__EMAIL = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
export const TEXT__VALIDATION_ERROR__USERNAME =
  'Must provide a valid email address.';
  export const TEXT__VALIDATION_ERROR__PASSWORD_EMPTY =
  'Must provide a password.';

  export const TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_EMPTY =
  'Must provide a confirm password.';
  
  export const TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_UNMATCH =
  'Confirm password must match with password.';
  
  export const URL_HOST =
  'http://mturbine.com'; //  'http://swscart.com/turantapi';


  export const ERROR_VALIDATIONS = {
    REGEX__EMAIL : /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
    TEXT__VALIDATION_ERROR__USERNAME : 'Must provide a valid email address.',
    TEXT__VALIDATION_ERROR__PASSWORD_EMPTY : 'Must provide a password.',
    TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_EMPTY : 'Must provide a confirm password.',
    TEXT__VALIDATION_ERROR__CONFIRM_PASSWORD_UNMATCH :'Confirm password must match with password.',
    TEXT__VALIDATION_ERROR__USERNAME_EMPTY : 'Please fill username'
  }



export const super_admin = ["Super Admin"];
export const admin = [ "Admin"];
export const customer_care = [ "Customer Care"];



export const ROUTE_VALIDATE = {
  IS_ADMIN  : true,
  IS_SUPER_ADMIN : true,
  IS_CUSTOMER_CARE  :true
}