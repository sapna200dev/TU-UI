import { setAuthHeader } from './axiosConfig';


/**
 * Handle after logout page
 */
export function redirectToLoginPage(logout = false) {
  if (logout) {
    setTimeout(() => {
      window.location.reload();
    }, 1500);
    localStorage.removeItem("access_token");
    localStorage.removeItem("username");
    localStorage.removeItem("role");
    localStorage.removeItem("loggedInUser");
    redirectToDashboard(false);
  }
}

/**
 * Handle auth routes
 */
export function redirectToDashboard(priv) {
  if (priv) {
    window.location.hash = "/dashboard";
  } else {
    window.location.hash = "/login";
  }
}

/**
 * Get user token from storage
 */
export function getUserToken(name) {
  const t = localStorage.access_token;
  return t ? t : "";
}

/**
 * Set user token to storage
 */
export function settToken(token) {
  localStorage.setItem("access_token", token);
  setAuthHeader();
}

/**
 * Set auth user role to storage
 */
export function settUserObj(user) {
  localStorage.setItem("role", user);
}

/**
 * Set auth user to storage
 */
export function setLoggedInUser(user) {
  localStorage.setItem("loggedInUser", user);
}

/**
 * Set auth user to storage
 */
export function getLoggedInUser() {
  return localStorage.loggedInUser;
}

/**
 * Set auth user to storage
 */
export function getUserObj() {
  return localStorage.role;
}

/**
 * Set user token to storage
 */
export function settUser(user) {
  localStorage.setItem("username", user);
}

/**
 * Get user token from storage
 */
export function getUsername(username) {
  const t = localStorage.username;
  return t ? t : "";
}




