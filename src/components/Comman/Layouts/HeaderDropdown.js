import React from "react";
import { connect } from "react-redux";
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { logout } from "../../Auth/servicers/common";
import { redirectToLoginPage, getUsername } from "../../Comman/functions";
import { URL_HOST } from "../../Comman/constants";

const TheHeaderDropdown = (props) => {
  const logoutUser = () => {
    console.log("logout action fire");
    logout();

    redirectToLoginPage(true);
  };

  let displayImg = "";

  const { profile, payload } = props;

  let image = props.payload && props.payload.image ? props.payload.image : null;

  console.log("logout action fire", profile);

  if (image != null) {
    displayImg = image;
  } else {
    displayImg = profile;
  }

  return (
    <CDropdown className="c-header-nav-items mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={`${URL_HOST}${displayImg}`} //{"avatars/6.jpg"}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>
            Loggedin as <b>{getUsername("username")}</b>
          </strong>
        </CDropdownItem>
        <CDropdownItem href="#/profile">
          <CIcon name="cil-user" className="mfe-2" />
          Profile
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-settings" className="mfe-2" />
          Settings
        </CDropdownItem>
        {/* <CDropdownItem>
          <CIcon name="cil-credit-card" className="mfe-2" />
          Payments
          <CBadge color="secondary" className="mfs-auto">
            42
          </CBadge>
        </CDropdownItem> */}
        {/* <CDropdownItem>
          <CIcon name="cil-file" className="mfe-2" /> 
          Projects
          <CBadge color="primary" className="mfs-auto">42</CBadge>
        </CDropdownItem> */}
        <CDropdownItem divider />
        <CDropdownItem onClick={logoutUser}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          Logout Account
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
};

export function mapStateToProps(state) {
  return {
    isLoading: state.ProfileReducer.isLoading,
    payload: state.ProfileReducer.payload,
    isSuccessMsg: state.ProfileReducer.isSuccessMsg,
  };
}

export default connect(mapStateToProps, null)(TheHeaderDropdown);
// export default TheHeaderDropdown;
