import React, { Component, Fragment } from "react";
import "./navbarStyle.css";
import { getUserObj } from "../../../components/Comman/functions";
import {
  BankSvgIcon,
  CustomerSvgIcon,
  DashboardSvgIcon,
} from "../../Comman/Icons";

export class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: "",
      routeMatch: "",
      id: "",
    };
  }

  componentDidMount() {
    const { id } = this.state;
    console.log("route = ", id);
    console.log("route  id = ", this.props);
    console.log(window.location);
    this.setState({ routeMatch: window.location.hash });
    // const { id } = this.props.match.params;

    const user_role = getUserObj("role");
    if (user_role) {
      this.setState({
        role: JSON.parse(user_role),
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { routeMatch } = this.state;
    console.log("route  window.location.hash = ", window.location.hash);
    if (prevProps.routeMatch != routeMatch) {
      console.log("route  window.location.hash = ", window.location.hash);
    }
  }
  render() {
    const { role, routeMatch } = this.state;
    const super_admin = ["Super Admin"];
    const admin = ["Admin"];
    const customer = ["Super Admin", "Customer"];

    let pathname = window.location.hash;

    var employee = pathname.substring(0, 10);
    const onEmployee = /\/employee/.test(employee);

    return (
      <ul
        className="c-sidebar-nav h-100 ps ps--active-y"
        style={{ position: "relative" }}
      >
        <li className="c-sidebar-nav-item">
          <a
            aria-current="page"
            className={`c-sidebar-nav-link   ${
              routeMatch == "#/dashboard" ? "c-active" : ""
            }`}
            href="#/dashboard"
          >
            <DashboardSvgIcon />
            Dashboard
            {/* <span className="badge badge-info">NEW</span> */}
          </a>
        </li>
        {!super_admin.includes(role.display_name) ? (
          <Fragment>
            {admin.includes(role.display_name) ? (
              <Fragment>
                <li className="c-sidebar-nav-title">Details</li>

                <li className={`c-sidebar-nav-item  `}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/employee" ? "c-active" : ""
                    }`}
                    href="#/employee"
                  >
                    <CustomerSvgIcon />
                    Manage Employee
                  </a>
                </li>
                <li className={`c-sidebar-nav-item  `}>
                  <a
                    className={`c-sidebar-nav-link   ${
                      routeMatch == "#/end-user" ? "c-active" : ""
                    }`}
                    href="#/end-user"
                  >
                    <CustomerSvgIcon />
                    Manage End User
                  </a>
                </li>
              </Fragment>
            ) : (
              <Fragment></Fragment>
            )}
          </Fragment>
        ) : (
          <Fragment>
            {customer.includes(role.display_name) ? (
              <Fragment>
                <li className="c-sidebar-nav-title">Details</li>
                <li className="c-sidebar-nav-item c-active">
                  <a className="c-sidebar-nav-link" href="#/bank-list">
                    <BankSvgIcon />
                    Bank
                  </a>
                </li>
              </Fragment>
            ) : null}
          </Fragment>
        )}

        <li className="c-sidebar-nav-divider m-2" />
        <div className="ps__rail-x" style={{ left: "0px", bottom: "-318px" }}>
          <div
            className="ps__thumb-x"
            tabIndex={0}
            style={{ left: "0px", width: "0px" }}
          />
        </div>
        <div
          className="ps__rail-y"
          style={{ top: "318px", right: "0px", height: "374px" }}
        >
          <div
            className="ps__thumb-y"
            tabIndex={0}
            style={{ top: "137px", height: "161px" }}
          />
        </div>
      </ul>
    );
  }
}
