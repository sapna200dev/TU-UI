import React, { Fragment } from "react";
import { CToast, CToastBody, CToastHeader, CToaster } from "@coreui/react";

export const ShowToastMessage = (props) => {
  const { success, header, content } = props;
    console.log('message = ', header)
  return (
    <Fragment>
      <CToaster position={"top-right"} style={{marginTop: '46px'}}>
        <CToast
          show={true}
          autohide={50000}
          fade={true}
        >
          <CToastHeader

            className={`${success ? "success_color" : "error_color"}`}
          >
            {header}
          </CToastHeader>
          <CToastBody
            className={`${success ? "success_color" : "error_color"}`}
          >
            {content}
          </CToastBody>
        </CToast>
      </CToaster>
    </Fragment>
  );
};
