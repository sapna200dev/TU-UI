import React from "react";
import {
  admin,
  super_admin,
  customer_care,
  ROUTE_VALIDATE,
} from "./components/Comman/constants";
import { getUserObj } from "./components/Comman/functions";
import {
  Bank,
  Banks,
  BankDetail,
  ShowBank,
} from "./components/Bank/containers";
import { Profile } from "./components/Profile/containers";
import { Dashboard } from "./components/Dashboard/containers";
import AddEmployee from "./components/Employee/containers/emaployee/AddEmployee";
import { Employee } from "./components/Employee/containers/emaployee/Employee";
import { ViewEmployee } from "./components/Employee/containers/emaployee/ViewEmployee";
import EmployeeDetails from "./components/Employee/containers/emaployee/EmployeeDetails";
import {EndUsers, AddEndUser, EditEndUser, EndUserDetails} from './components/EndUser/container/EndUser/index'

let is_admin = false;
let is_super_admin = false; 
let is_customer_care = false;

const user_role = getUserObj("role");
let role = "";
if (user_role) {
  role = JSON.parse(user_role);
}

if (admin.includes(role.display_name)) {
  is_admin = ROUTE_VALIDATE.IS_ADMIN;
} else if (super_admin.includes(role.display_name)) {
  is_super_admin = ROUTE_VALIDATE.IS_SUPER_ADMIN;
} else if (customer_care.includes(role.display_name)) {
  is_customer_care = ROUTE_VALIDATE.IS_CUSTOMER_CARE;
}

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/profile", name: "Profile", component: Profile },
  {
    path: `${is_super_admin ? "/bank-list" : "/dashboard"}`,
    name: "Bank List",
    component: Banks,
  },
  {
    path: `${is_super_admin ? "/bank-add" : "/dashboard"}`,
    name: "Bank Add",
    component: Bank,
  },

  {
    path: `${is_super_admin ? "/bank-update/:id" : "/dashboard"}`,
    name: "Bank Edit",
    component: BankDetail,
  },

  {
    path: `${is_super_admin ? "/bank-view/:id" : "/dashboard"}`,
    name: "Bank View",
    component: ShowBank,
  },

  {
    path: `${is_admin ? "/employee" : "/dashboard"}`,
    name: "Employee List",
    component: Employee,
  },
  {
    path: `${is_admin ? "/employee-add" : "/dashboard"}`,
    name: "Employee Add",
    component: AddEmployee,
  },
  {
    path: `${is_admin ? "/employee-view/:id" : "/dashboard"}`,
    name: "Employee View",
    component: ViewEmployee,
  },
  {
    path: `${is_admin ? "/employee-edit/:id" : "/dashboard"}`,
    name: "Employee Edit",
    component: EmployeeDetails,
  },



  {
    path: `${is_admin ? "/end-user" : "/dashboard"}`,
    name: "End Users",
    component: EndUsers,
  },
  {
    path: `${is_admin ? "/end-user-add" : "/dashboard"}`,
    name: "Add End User",
    component: AddEndUser,
  },

  {
    path: `${is_admin ? "/end-user-edit/:id" : "/dashboard"}`,
    name: "Edit End User",
    component: EditEndUser,
  },
  {
    path: `${is_admin ? "/end-user-view/:id" : "/dashboard"}`,
    name: "End User Details",
    component: EndUserDetails,
  },
  // { path: '/bank/add', name: 'Add', component: Bank },
  // { path: '/bank/update/:id', name: 'Edit', component: BankDetail },
  // { path: '/bank/view/:id', name: 'View', component: ShowBank },
  // { path: '/customer', name: 'Customer', component: Customer },
  // { path: '/customer-add', name: 'Add', component: AddCustomer },
  // { path: "/customer-view/:id", name: "View", component: ShowCustomer },
  // { path: '/customer-update/:id', name: 'Update', component: CustomerDetails },


];

export default routes;
