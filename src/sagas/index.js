import { all, fork } from "redux-saga/effects";

// LOGIN
import { watchLogin } from "../components/Auth/sagas/index";
import {
  watchFetchRoles,
  watchFetchBranches,
  watchFetchStatus,
} from "../components/Comman/sagas/index";
import { watchProfileSaga } from "../components/Profile/sagas/index";
import {
  watchFetchTeamLeadsSaga,
  watchFetchSaleAgentsSaga,
  watchFetchLoanTypeSaga,
  watchFetchLanguagesSaga,
  watchFetchEndUsersSaga
} from "../components/EndUser/sagas/index";

export default function* sagas() {
  yield all([
    fork(watchLogin),
    fork(watchFetchRoles),
    fork(watchFetchBranches),
    fork(watchFetchStatus),
    fork(watchProfileSaga),
    fork(watchFetchTeamLeadsSaga),
    fork(watchFetchSaleAgentsSaga),
    fork(watchFetchLoanTypeSaga),
    fork(watchFetchLanguagesSaga),
    fork(watchFetchEndUsersSaga)
  ]);
}
