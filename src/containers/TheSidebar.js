import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import logo from '../assets/images/logo_1.jpeg'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'

import CIcon from '@coreui/icons-react'
import {Navbar} from '../components/Comman/components/Navbar'

// sidebar nav config
import navigation from './_nav'

const TheSidebar = (props) => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)

  console.log('sidebar =', props )


  return ( 
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({type: 'set', sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        {/* <h4>TURANT APP</h4> */}
        <img src={logo} style={{ height: '59px',    width: '100%' }}/>
         {/* <CIcon
          className="c-sidebar-brand-full"
          name="logo"
          height={35}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="logo"
          height={35}
        />  */}
      </CSidebarBrand>
      <CSidebarNav>

        {/* <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />  */}
        <Navbar />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
