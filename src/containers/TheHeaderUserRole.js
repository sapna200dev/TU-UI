import React, { useEffect, useState, Fragment } from "react";
import { getUserObj } from "../components/Comman/functions";

const TheHeaderUserRole = () => {
  const [role, setRole] = useState("");

  useEffect(() => {
    const user_role = getUserObj("role");
    if (user_role) {
      setRole(JSON.parse(user_role));
    }
  }, []);

  return (
    <Fragment>
      <div className="text-truncate font-weight-bold">
        <span className="fa fa-exclamation text-danger"></span>Turant {role.display_name}
      </div>
    </Fragment>
  );
};

export default TheHeaderUserRole;
